<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../ui/setup.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="30"/>
        <source>My Sets</source>
        <translation>Settaggi</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="42"/>
        <source>Settings</source>
        <translation>Settaggi</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="54"/>
        <source>Terminal</source>
        <translation>Teminale</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="67"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="80"/>
        <source>Syst. ListPkg</source>
        <translation>Lista Pkg</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="113"/>
        <source>Tmp Pkg-Dir</source>
        <translation>Dir Pkg Temp</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="132"/>
        <source>Mirrors Upd</source>
        <translation>Agg. Mirrors</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="213"/>
        <source>Pkg Repos.</source>
        <translation>Pkg Deposito.</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="260"/>
        <source>Aspect</source>
        <translation>Aspetto</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="291"/>
        <source>Light</source>
        <translation>Chiaro</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="304"/>
        <source>Dark</source>
        <translation>Scuro</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="311"/>
        <source>Repository</source>
        <translation>Deposito</translation>
    </message>
    <message>
        <location filename="../../ui/setup.ui" line="359"/>
        <source>Mirror:</source>
        <translation>Mirror:</translation>
    </message>
</context>
<context>
    <name>PkFiles</name>
    <message>
        <location filename="../../src/pkfiles.cpp" line="53"/>
        <source>List Files of %1</source>
        <translation>Elenco file di %1</translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="122"/>
        <location filename="../../src/pkfiles.cpp" line="171"/>
        <source>Open File </source>
        <translation>Apri File </translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="123"/>
        <source>File %1 is not a program! 
Use right click to open Terminal or FileBrowser</source>
        <translation>Il file %1 non è un programma! 
Utilizzare il pulsante destro del mouse per aprire Terminale o FileBrowser</translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="128"/>
        <source>Run Program File</source>
        <translation>Esegui File</translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="129"/>
        <source>Try to Run File %1 
 in %2 </source>
        <translation>Provo a eseguire il file %1 
in %2 </translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="139"/>
        <source>Open Program File </source>
        <translation>Apri Programma </translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="140"/>
        <location filename="../../src/pkfiles.cpp" line="171"/>
        <source>File %1 not exist !</source>
        <translation>Il file %1 non esiste !</translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="152"/>
        <source>Open terminal here</source>
        <translation>Apri il terminale qui</translation>
    </message>
    <message>
        <location filename="../../src/pkfiles.cpp" line="153"/>
        <source>Open directory here</source>
        <translation>Apri la directory qui</translation>
    </message>
</context>
<context>
    <name>PkFindFile</name>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="40"/>
        <source>Find File</source>
        <translation>Cerca File</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="55"/>
        <source>Find File inside Packages</source>
        <translation>Cerca un File dentro i Pacchetti</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="67"/>
        <source>Package</source>
        <translation>Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="67"/>
        <source>File Name</source>
        <translation>Nome File</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="78"/>
        <source>Find</source>
        <translation>Trova</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="85"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="109"/>
        <source>Info </source>
        <translation>Informazioni </translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="109"/>
        <source>Enter more than three chars..</source>
        <translation>Immettere più di tre caratteri.</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="148"/>
        <source>--Nothing found--</source>
        <translation>--Non è stato trovato nessun File--</translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="153"/>
        <source>Find %1 items contains : %2 </source>
        <translation>Trovati %1 elementi che contiengono : %2 </translation>
    </message>
    <message>
        <location filename="../../src/pkfindfile.cpp" line="168"/>
        <source>Searching File in Packages..</source>
        <translation>Ricerca File nei pacchetti..</translation>
    </message>
</context>
<context>
    <name>PkFindPkg</name>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="30"/>
        <location filename="../../src/pkfindpkg.cpp" line="44"/>
        <source>Find Package</source>
        <translation>Trova Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="55"/>
        <source> </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="61"/>
        <source>Sub string</source>
        <translation>Sottostringa</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="67"/>
        <source>Show Packages</source>
        <translation>Mostra Pacchetti</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="75"/>
        <source>Find</source>
        <translation>Trova</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="81"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="99"/>
        <location filename="../../src/pkfindpkg.cpp" line="119"/>
        <source>Info </source>
        <translation>Informazioni </translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="99"/>
        <source>Enter more than three chars..</source>
        <translation>Immettere più di tre caratteri..</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="115"/>
        <source>Num: %1 Package Found</source>
        <translation>Num: %1 Pacchetto non trovato</translation>
    </message>
    <message>
        <location filename="../../src/pkfindpkg.cpp" line="119"/>
        <source>No Packages Found !</source>
        <translation>Nessun Pachetto trovaro !</translation>
    </message>
</context>
<context>
    <name>PkInfo</name>
    <message>
        <location filename="../../src/pkinfo.cpp" line="78"/>
        <source> URL not found</source>
        <translation> URL non trovato</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="90"/>
        <source>&lt;h4&gt;Summary:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Sommario:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="94"/>
        <source>&lt;h4&gt;Version:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Versione:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="98"/>
        <source>&lt;h4&gt;Architecture:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Architettura:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="102"/>
        <source>&lt;h4&gt;Group:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Grouppo:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="106"/>
        <source>&lt;h4&gt;Package Size:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Dimensione Pacchetto:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="110"/>
        <source>&lt;h4&gt;Files Size:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Dimensione Files:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="114"/>
        <source>&lt;h4&gt;Description:&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Descrizione:&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="127"/>
        <source>Open Info of </source>
        <translation>Apri informazioni di </translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="131"/>
        <source>Package &apos;info&apos; empty !</source>
        <translation>Informazioni Pacchetto Vuota !</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="136"/>
        <source>No Package recognize</source>
        <translation>Pacchetto non riconosciuto</translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="143"/>
        <source>Click to Open URL </source>
        <translation>Fai clic per aprire l&apos;URL </translation>
    </message>
    <message>
        <location filename="../../src/pkinfo.cpp" line="148"/>
        <source>Open URL </source>
        <translation>Apri URL </translation>
    </message>
</context>
<context>
    <name>PkMainWindow</name>
    <message>
        <location filename="../../src/pkmwform.cpp" line="52"/>
        <source>Search Package: </source>
        <translation>Ricerca Pacchetto: </translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="58"/>
        <source>Num Package Installed</source>
        <translation>Num Pacchetti Installati</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="68"/>
        <source>Packages Installed</source>
        <translation>Pacchetti installati</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="73"/>
        <source>Packages Uninstalled</source>
        <translation>Pacchetti Rimossi</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="87"/>
        <source>System Information</source>
        <translation>Informazioni di Sistema</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="91"/>
        <source>Package Info</source>
        <translation>Info Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="95"/>
        <source>Package Files</source>
        <translation>File Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="119"/>
        <source>&amp;Open</source>
        <translation>&amp;Apri</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="121"/>
        <source>Alt+o</source>
        <translation>Alt+a</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="122"/>
        <source>Open package file</source>
        <translation>Apri il file del pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="125"/>
        <source>&amp;Remove</source>
        <translation>&amp;Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="127"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="128"/>
        <source>Remove package file</source>
        <translation>Rimuovere pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="131"/>
        <source>&amp;Reload</source>
        <translation>R&amp;eload</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="133"/>
        <source>Alt+r</source>
        <translation>Alt+e</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="134"/>
        <source>Reload Package</source>
        <translation>Ricarica pacchetti</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="137"/>
        <source>&amp;Delete</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="139"/>
        <source>Alt+d</source>
        <translation>Alt+c</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="140"/>
        <location filename="../../src/pkmwform.cpp" line="230"/>
        <source>Delete Package</source>
        <translation>Cancella Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="144"/>
        <source>&amp;Search Package </source>
        <translation>Cerca &amp;Pacchetto </translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="146"/>
        <source>Alt+p</source>
        <translation>Alt+p</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="147"/>
        <location filename="../../src/pkmwform.cpp" line="235"/>
        <source>Search Package</source>
        <translation>Cerca pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="150"/>
        <source>&amp;Search File </source>
        <translation>Ricerca &amp;File </translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="152"/>
        <source>Alt+f</source>
        <translation>Alt+f</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="153"/>
        <location filename="../../src/pkmwform.cpp" line="239"/>
        <source>Search File</source>
        <translation>Ricerca File</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="156"/>
        <source>&amp;Setup</source>
        <translation>Se&amp;tup</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="158"/>
        <source>Set Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="161"/>
        <source>&amp;About</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="163"/>
        <source>Show the application&apos;s About box</source>
        <translation>Visualizzare la casella Informazioni sull&apos;applicazione</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="166"/>
        <location filename="../../src/pkmwform.cpp" line="202"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="167"/>
        <source>Show the application&apos;s Help</source>
        <translation>Visualizzare la Guida dell&apos;applicazione</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="171"/>
        <source>About &amp;Qt</source>
        <translation>About &amp;Qt</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="172"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Visualizzare la casella Informazioni sulla libreria Qt</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="175"/>
        <source>&amp;Hide</source>
        <translation>&amp;Nascondi</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="179"/>
        <source>&amp;Quit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="181"/>
        <source>Alt+q</source>
        <translation>Alt+e</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="182"/>
        <source>Quit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="188"/>
        <source>&amp;Main</source>
        <translation>&amp;Menu</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="199"/>
        <source>&amp;Settings</source>
        <translation>&amp;Impostazioni</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="218"/>
        <source>Package Browser</source>
        <translation>Esplora Pacchetti</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="222"/>
        <source>Package Remove</source>
        <translation>Rimuovi Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="226"/>
        <source>Reload Package List</source>
        <translation>Ricarica Lista Pkg</translation>
    </message>
    <message>
        <location filename="../../src/pkmwform.cpp" line="243"/>
        <source>Upload</source>
        <translation>Ghost</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="53"/>
        <source>Set Font %1 size %2</source>
        <translation>Imposta font %1 dimensione %2</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="66"/>
        <source>About...</source>
        <translation>About...</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="173"/>
        <source>Install Packages</source>
        <translation>Installa Pacchetti</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="173"/>
        <source>Package *.txz *.tgz *.tlz (*.txz *.tgz *.tlz)</source>
        <translation>Pacchetto *.txz *.tgz *.tlz (*.txz *.tgz *.tlz)</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="186"/>
        <location filename="../../src/pkmwfunc.cpp" line="198"/>
        <location filename="../../src/pkmwfunc.cpp" line="223"/>
        <location filename="../../src/pkmwfunc.cpp" line="247"/>
        <location filename="../../src/pkmwfunc.cpp" line="277"/>
        <location filename="../../src/pkmwfunc.cpp" line="286"/>
        <source>Info </source>
        <translation>Informazioni </translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="186"/>
        <source>No Packages Selected !</source>
        <translation>Nessun pacchetto selezionato!</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="198"/>
        <source>Select Packages !</source>
        <translation>Seleziona Pacchetti !</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="223"/>
        <location filename="../../src/pkmwfunc.cpp" line="247"/>
        <source>Select Packages First !</source>
        <translation>Seleziona prima i pacchetti !</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="277"/>
        <source>Unable to read package information %1!
</source>
        <translation>Impossibile leggere le informazioni pacchetto %1!
</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="286"/>
        <source>Unable to read the list of files %1 !</source>
        <translation>Impossibile leggere l&apos;elenco dei file di %1 !</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="309"/>
        <source>Packages Num.: %1</source>
        <translation>Num. Pacchetti: %1</translation>
    </message>
    <message>
        <source>Packages: %1</source>
        <translation type="obsolete">Tot Pacchetti %1</translation>
    </message>
    <message>
        <source>Package: %1</source>
        <translation type="vanished">Pacchetti %1</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="374"/>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="380"/>
        <source>SysInfo</source>
        <translation>Info Sistema</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="386"/>
        <source>Mirror</source>
        <translation>Mirror</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="392"/>
        <source>Repository</source>
        <translation>Deposito</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="476"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../../src/pkmwfunc.cpp" line="494"/>
        <source>About</source>
        <translation>About</translation>
    </message>
</context>
<context>
    <name>PkManage</name>
    <message>
        <location filename="../../src/pkmanage.cpp" line="57"/>
        <source>&amp;Close</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="58"/>
        <source>&amp;Stop</source>
        <translation>&amp;Stop</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="61"/>
        <source>Upgrade Package</source>
        <translation>Aggiorna Pachetto</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="62"/>
        <source>Test (warn)</source>
        <translation>Prova(warn)</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="63"/>
        <source>Forcefully </source>
        <translation>Forza (-f) </translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="64"/>
        <source>Keep this window</source>
        <translation>Mantieni questa finestra</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="65"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="67"/>
        <source>PACKAGES</source>
        <translation>PACCHETTI</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="329"/>
        <source>Start %1 with process ID: %2
</source>
        <translation>Inizio %1 con ID processo: %2
</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="382"/>
        <source>WARNING: STOP !!
</source>
        <translation>ATTENZIONE: STOP!!
</translation>
    </message>
    <message>
        <location filename="../../src/pkmanage.cpp" line="385"/>
        <source>%1 with process ID: %2 as stopped !!
</source>
        <translation>%1 con ID processo: %2 è stato interrotto !!
</translation>
    </message>
</context>
<context>
    <name>PkManageDnl</name>
    <message>
        <location filename="../../src/pkmanagednl.cpp" line="45"/>
        <source>&amp;Abort</source>
        <translation>&amp;Abort</translation>
    </message>
    <message>
        <location filename="../../src/pkmanagednl.cpp" line="46"/>
        <source>&amp;Close</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../../src/pkmanagednl.cpp" line="49"/>
        <source>PACKAGES</source>
        <translation>PACCHETTI</translation>
    </message>
    <message>
        <location filename="../../src/pkmanagednl.cpp" line="52"/>
        <source>Keep this window</source>
        <translation>Mantieni questa finestra</translation>
    </message>
    <message>
        <location filename="../../src/pkmanagednl.cpp" line="53"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
</context>
<context>
    <name>PkNetFunc</name>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="128"/>
        <source>Mirrors Urls file save ! </source>
        <translation>Lista Mirrors Salvata ! </translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="268"/>
        <source>%1 file save ! </source>
        <translation>%1 File salvato ! </translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="321"/>
        <source> %1 of %2 files downloaded successfully 
</source>
        <translation> %1 di %2 file scaricati correttamente 
</translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="341"/>
        <source>Problem save file %1 for download %2: %3</source>
        <translation>Attenzione salvataggio file %1 per il download %2: %3</translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="360"/>
        <source>Downloading %1</source>
        <translation>Scaricamento %1</translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="441"/>
        <source>Stop Download by User. Delete %1 
</source>
        <translation>Interrotto il download da parte dell&apos;utente. Elimina %1 
</translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="452"/>
        <source>Failed: %1</source>
        <translation>Fallito %1</translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="466"/>
        <source>Succeeded.</source>
        <translation>Riuscito.</translation>
    </message>
    <message>
        <location filename="../../src/pknetfunc.cpp" line="499"/>
        <source>Request: %1 was redirected with code: %2
Redirected to: %3</source>
        <translation>Richiesta: %1 è stato reindirizzato con il codice: %2
Reindiretto a: %3</translation>
    </message>
</context>
<context>
    <name>PkSearchLine</name>
    <message>
        <location filename="../../src/pksearchline.cpp" line="35"/>
        <source>Clears the text</source>
        <translation>Cancella il Testo</translation>
    </message>
</context>
<context>
    <name>PkSetup</name>
    <message>
        <location filename="../../src/pksetup.cpp" line="68"/>
        <source>SetUp</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="121"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="122"/>
        <source>Discard</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="123"/>
        <source>Reload</source>
        <translation>Ricarica</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="125"/>
        <source>Search Dir</source>
        <translation>Seleziona Dir</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="136"/>
        <source>Select directory</source>
        <translation>Seleziona Directory</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="241"/>
        <source>Reload !</source>
        <translation>Ricarica !</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="263"/>
        <source> not found.</source>
        <translation> non trovato.</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="264"/>
        <source>Please Force Reload It </source>
        <translation>Forza lo Scaricamento </translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="294"/>
        <source> Country:       Adderss:</source>
        <translation> Paese:           Indirizzo:</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="330"/>
        <source>Every Start</source>
        <translation>Ogni Avvio</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="334"/>
        <source>Every Year</source>
        <translation>Ogni Anno</translation>
    </message>
    <message>
        <location filename="../../src/pksetup.cpp" line="338"/>
        <source>%1 Days</source>
        <translation>%1 Giorni</translation>
    </message>
</context>
<context>
    <name>PkTreeView</name>
    <message>
        <location filename="../../src/pktreeview.cpp" line="147"/>
        <source>Package</source>
        <translation>Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="148"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="149"/>
        <source>Mark</source>
        <translation>Selez</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="150"/>
        <source>Date_Hide</source>
        <translation>Date_Hide</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="172"/>
        <source>Load Packages List..from </source>
        <translation>Carica l&apos;elenco dei pacchetti.. Da </translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="284"/>
        <source>Resize </source>
        <translation>Ridimensiona </translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="285"/>
        <source>Resize by Content</source>
        <translation>Ridimensiona per Contenuto</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="290"/>
        <source>New First</source>
        <translation>Nuovo</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="291"/>
        <source>Old First</source>
        <translation>Vecchio</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="297"/>
        <source>Uncheck All</source>
        <translation>Deseleziona Tutti</translation>
    </message>
    <message>
        <location filename="../../src/pktreeview.cpp" line="298"/>
        <source>uncheck</source>
        <translation>Deseleziona</translation>
    </message>
</context>
<context>
    <name>PkUpgTreeView</name>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="108"/>
        <source>Package</source>
        <translation>Pacchetto</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="109"/>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="110"/>
        <source>Mark</source>
        <translation>Selez</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="134"/>
        <source>Load Packages List..from </source>
        <translation>Carica l&apos;elenco dei pacchetti.. Da </translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="169"/>
        <source>Old Pkg</source>
        <translation>Pkg Vecchio</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="174"/>
        <source>Same Pkg</source>
        <translation>Pkg Uguale</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="179"/>
        <source>New Pkg</source>
        <translation>Pkg Nuovo</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="184"/>
        <source>Not Inst.</source>
        <translation>Pkg Assent.</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="230"/>
        <location filename="../../src/pkupgtreeview.cpp" line="413"/>
        <source>Load XML File Problem</source>
        <translation>Problema di caricamento del file XML</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="231"/>
        <source>Couldn&apos;t open %1 to load repository packages</source>
        <translation>Impossibile aprire %1 per caricare i pacchetti del repository</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="414"/>
        <source>Couldn&apos;t open %1 to load local packages</source>
        <translation>Impossibile aprire %1 per caricare i pacchetti locali</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="463"/>
        <source>Resize </source>
        <translation>Ridimensiona </translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="464"/>
        <source>Resize by Content</source>
        <translation>Ridimensiona per Contenuto</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="469"/>
        <source>New </source>
        <translation>Nuovo </translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="470"/>
        <source>Old </source>
        <translation>Vecchio </translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="476"/>
        <source>Uncheck All</source>
        <translation>Deseleziona Tutti</translation>
    </message>
    <message>
        <location filename="../../src/pkupgtreeview.cpp" line="477"/>
        <source>uncheck</source>
        <translation>Deseleziona</translation>
    </message>
</context>
<context>
    <name>PkUtility</name>
    <message>
        <location filename="../../src/pkutility.cpp" line="53"/>
        <source>File %1 not found</source>
        <translation>File %1 non trovato</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/main.cpp" line="60"/>
        <source>Systray</source>
        <translation>Systray</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="61"/>
        <source>I couldn&apos;t detect any system tray on this system.</source>
        <translation>Non sono riuscito a rilevare alcuna barra delle applicazioni su questo sistema.</translation>
    </message>
    <message>
        <location filename="../../src/pkconstants.h" line="179"/>
        <source>The application is already running.
Allowed to run only one instance of the application.</source>
        <translation>L&apos;applicazione è già in esecuzione. 
Consentito l&apos;esecuzione di una sola istanza dell&apos;applicazione.</translation>
    </message>
    <message>
        <location filename="../../src/pkconstants.h" line="185"/>
        <source>PkManager:  Slackware Package Tool.
 </source>
        <translation>PkManager: Slackware Package Tool.
 </translation>
    </message>
</context>
</TS>
