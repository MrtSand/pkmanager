/********************************************************************************
** Form generated from reading UI file 'setup.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETUP_H
#define UI_SETUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QTabWidget *tbSetup;
    QWidget *tab;
    QGroupBox *groupBox;
    QLabel *lbConsole;
    QToolButton *btSelDir;
    QLabel *lbListPk;
    QLineEdit *leListPkg;
    QLineEdit *leDirTmp;
    QLabel *lbDirPk;
    QLabel *lbUpD;
    QSlider *swUpd;
    QLabel *lbDays;
    QLabel *lbVersion;
    QLineEdit *leVersion;
    QCheckBox *cbVersion;
    QLabel *lbTerminal;
    QGroupBox *groupBox_2;
    QSlider *swTheme;
    QLabel *lbThemeL;
    QLabel *lbThemeB;
    QWidget *tab_2;
    QTableWidget *mirrorList;
    QLabel *lbUpldate;
    QLabel *lbMirSelect;
    QLabel *lbMir;
    QPushButton *btForce;
    QPushButton *btDis;
    QPushButton *btOk;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(640, 480);
        tbSetup = new QTabWidget(Dialog);
        tbSetup->setObjectName(QString::fromUtf8("tbSetup"));
        tbSetup->setGeometry(QRect(10, 20, 611, 411));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 581, 221));
        lbConsole = new QLabel(groupBox);
        lbConsole->setObjectName(QString::fromUtf8("lbConsole"));
        lbConsole->setGeometry(QRect(10, 30, 90, 25));
        btSelDir = new QToolButton(groupBox);
        btSelDir->setObjectName(QString::fromUtf8("btSelDir"));
        btSelDir->setGeometry(QRect(470, 60, 28, 25));
        lbListPk = new QLabel(groupBox);
        lbListPk->setObjectName(QString::fromUtf8("lbListPk"));
        lbListPk->setGeometry(QRect(10, 90, 90, 25));
        leListPkg = new QLineEdit(groupBox);
        leListPkg->setObjectName(QString::fromUtf8("leListPkg"));
        leListPkg->setGeometry(QRect(120, 90, 341, 25));
        leDirTmp = new QLineEdit(groupBox);
        leDirTmp->setObjectName(QString::fromUtf8("leDirTmp"));
        leDirTmp->setGeometry(QRect(120, 60, 341, 25));
        lbDirPk = new QLabel(groupBox);
        lbDirPk->setObjectName(QString::fromUtf8("lbDirPk"));
        lbDirPk->setGeometry(QRect(10, 60, 90, 25));
        lbUpD = new QLabel(groupBox);
        lbUpD->setObjectName(QString::fromUtf8("lbUpD"));
        lbUpD->setGeometry(QRect(10, 180, 101, 25));
        lbUpD->setLayoutDirection(Qt::LeftToRight);
        lbUpD->setAutoFillBackground(false);
        lbUpD->setScaledContents(false);
        lbUpD->setWordWrap(true);
        swUpd = new QSlider(groupBox);
        swUpd->setObjectName(QString::fromUtf8("swUpd"));
        swUpd->setGeometry(QRect(120, 180, 341, 25));
        swUpd->setCursor(QCursor(Qt::ArrowCursor));
        swUpd->setMaximum(365);
        swUpd->setPageStep(1);
        swUpd->setValue(0);
        swUpd->setTracking(true);
        swUpd->setOrientation(Qt::Horizontal);
        swUpd->setInvertedAppearance(false);
        swUpd->setInvertedControls(false);
        swUpd->setTickPosition(QSlider::NoTicks);
        lbDays = new QLabel(groupBox);
        lbDays->setObjectName(QString::fromUtf8("lbDays"));
        lbDays->setGeometry(QRect(470, 180, 101, 31));
        lbDays->setLayoutDirection(Qt::LeftToRight);
        lbDays->setAutoFillBackground(false);
        lbDays->setScaledContents(false);
        lbDays->setWordWrap(true);
        lbVersion = new QLabel(groupBox);
        lbVersion->setObjectName(QString::fromUtf8("lbVersion"));
        lbVersion->setGeometry(QRect(10, 120, 90, 25));
        leVersion = new QLineEdit(groupBox);
        leVersion->setObjectName(QString::fromUtf8("leVersion"));
        leVersion->setGeometry(QRect(120, 120, 341, 25));
        cbVersion = new QCheckBox(groupBox);
        cbVersion->setObjectName(QString::fromUtf8("cbVersion"));
        cbVersion->setGeometry(QRect(470, 120, 86, 25));
        lbTerminal = new QLabel(groupBox);
        lbTerminal->setObjectName(QString::fromUtf8("lbTerminal"));
        lbTerminal->setGeometry(QRect(120, 30, 341, 25));
        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 240, 581, 131));
        swTheme = new QSlider(groupBox_2);
        swTheme->setObjectName(QString::fromUtf8("swTheme"));
        swTheme->setGeometry(QRect(65, 30, 40, 25));
        swTheme->setMaximum(1);
        swTheme->setPageStep(1);
        swTheme->setOrientation(Qt::Horizontal);
        lbThemeL = new QLabel(groupBox_2);
        lbThemeL->setObjectName(QString::fromUtf8("lbThemeL"));
        lbThemeL->setGeometry(QRect(10, 30, 50, 25));
        lbThemeB = new QLabel(groupBox_2);
        lbThemeB->setObjectName(QString::fromUtf8("lbThemeB"));
        lbThemeB->setGeometry(QRect(120, 30, 50, 25));
        tbSetup->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        mirrorList = new QTableWidget(tab_2);
        mirrorList->setObjectName(QString::fromUtf8("mirrorList"));
        mirrorList->setGeometry(QRect(20, 30, 570, 321));
        lbUpldate = new QLabel(tab_2);
        lbUpldate->setObjectName(QString::fromUtf8("lbUpldate"));
        lbUpldate->setGeometry(QRect(20, 0, 471, 31));
        lbMirSelect = new QLabel(tab_2);
        lbMirSelect->setObjectName(QString::fromUtf8("lbMirSelect"));
        lbMirSelect->setGeometry(QRect(70, 350, 521, 31));
        lbMir = new QLabel(tab_2);
        lbMir->setObjectName(QString::fromUtf8("lbMir"));
        lbMir->setGeometry(QRect(20, 350, 51, 31));
        btForce = new QPushButton(tab_2);
        btForce->setObjectName(QString::fromUtf8("btForce"));
        btForce->setGeometry(QRect(510, 3, 80, 25));
        tbSetup->addTab(tab_2, QString());
        btDis = new QPushButton(Dialog);
        btDis->setObjectName(QString::fromUtf8("btDis"));
        btDis->setGeometry(QRect(540, 440, 81, 25));
        btOk = new QPushButton(Dialog);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setGeometry(QRect(440, 440, 81, 25));

        retranslateUi(Dialog);

        tbSetup->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "Dialog", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Dialog", "Settings", nullptr));
        lbConsole->setText(QCoreApplication::translate("Dialog", "Terminal", nullptr));
        btSelDir->setText(QCoreApplication::translate("Dialog", "...", nullptr));
        lbListPk->setText(QCoreApplication::translate("Dialog", "Syst. ListPkg", nullptr));
        lbDirPk->setText(QCoreApplication::translate("Dialog", "Tmp Pkg-Dir", nullptr));
        lbUpD->setText(QCoreApplication::translate("Dialog", "Mirrors Upd", nullptr));
        lbDays->setText(QString());
        lbVersion->setText(QCoreApplication::translate("Dialog", "Pkg Repos.", nullptr));
        lbTerminal->setText(QString());
        groupBox_2->setTitle(QCoreApplication::translate("Dialog", "Aspect", nullptr));
        lbThemeL->setText(QCoreApplication::translate("Dialog", "Light", nullptr));
        lbThemeB->setText(QCoreApplication::translate("Dialog", "Dark", nullptr));
        tbSetup->setTabText(tbSetup->indexOf(tab), QCoreApplication::translate("Dialog", "My Sets", nullptr));
        lbUpldate->setText(QString());
        lbMirSelect->setText(QString());
        lbMir->setText(QCoreApplication::translate("Dialog", "Mirror:", nullptr));
        btForce->setText(QString());
        tbSetup->setTabText(tbSetup->indexOf(tab_2), QCoreApplication::translate("Dialog", "Repository", nullptr));
        btOk->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETUP_H
