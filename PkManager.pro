#/************************************************************************
#*   PkManager Package Manager for Slackware                             *
#*   Copyright (C)   MrtSand                                             *
#*   https://gitlab.com/MrtSand/pkmanager.git                            *
#*                                                                       *
#*   This program is free software; you can redistribute it and/or       *
#*   modify it under the terms of the GNU General Public License         *
#*   as published by the Free Software Foundation; either version 2      *
#*   of the License, or (at your option) any later version.              *
#*                                                                       *
#*   This program is distributed in the hope that it will be useful,     *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
#*   GNU General Public License for more details.                        *
#*                                                                       *
#*   You should have received a copy of the GNU General Public License   *
#*   along with this program; if not, write to the Free Software         *
#*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
#*   Boston, MA  02110-1301, USA.                                        *
#************************************************************************/


QT += core gui network widgets concurrent

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

#CONFIG += debug_and_release

QMAKE_CXXFLAGS += -std=gnu++11

TARGET = pkmanager
TEMPLATE = app

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += \
    src/pkmainwindow.h \
    src/pkmanagednl.h \
    src/pknetfunc.h \
    src/pksearchline.h \
    src/pkstatusbar.h \
    src/pkconstants.h \
    src/pktreeview.h \
    src/pkfiles.h \
    src/pkinfo.h \
    src/pkinterface.h \
    src/pksetup.h \
    src/pkmanage.h \
    src/pkfindpkg.h \
    src/pkfindfile.h \
    src/pkupgtreeview.h \
    src/pkutility.h

SOURCES += \
    src/main.cpp \
    src/pkmainwindow.cpp \
    src/pkmanagednl.cpp \
    src/pkmwform.cpp \
    src/pkmwfunc.cpp \
    src/pknetfunc.cpp \
    src/pksearchline.cpp \
    src/pkstatusbar.cpp \
    src/pktreeview.cpp \
    src/pkfiles.cpp \
    src/pkinfo.cpp \
    src/pkinterface.cpp \
    src/pksetup.cpp \
    src/pkmanage.cpp \
    src/pkfindpkg.cpp \
    src/pkfindfile.cpp \
    src/pkupgtreeview.cpp \
    src/pkutility.cpp

FORMS += \
    ui/setup.ui

RESOURCES += \
    rc/pkmanager.qrc

DEPENDPATH += . \
    src\
    ui
INCLUDEPATH += . \
    src\
    ui

TRANSLATIONS +=  \
  rc/translations/PkManager_en_US.ts \
  rc/translations/PkManager_it_IT.ts

DISTFILES += \
    rc/translations/PkManager_en_US.ts \
    rc/translations/PkManager_it_IT.ts

UI_DIR = ui
MOC_DIR = build/moc
RCC_DIR = build/rcc
OBJECTS_DIR = build/obj
DESTDIR += bin

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INSTALLS += 
#QMAKE_CXXFLAGS += -std=c++0x
# these seem to be needed for building without linking problems on multilib...
#contains(QT_ARCH, x86_64): {
#    QMAKE_PREFIX_SHLIB = lib64
#    QMAKE_PREFIX_STATICLIB = lib64
#    QMAKE_LIBDIR_OPENGL_ES1 = $$QMAKE_LIBDIR_OPENGL
#    QMAKE_LIBDIR_OPENGL_ES2 = $$QMAKE_LIBDIR_OPENGL
#}


