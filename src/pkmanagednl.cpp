/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pkmanagednl.h"
#include "pkconstants.h"

PkManageDnl::PkManageDnl(QWidget *parent) : QDialog(parent)
{
    pkNetFunc = new PkNetFunc();
    createForm();
    setModal(true);
}

PkManageDnl::~PkManageDnl()
{}

void PkManageDnl::createForm()
{
    setWindowIcon(QIcon(":/manage"));
    title = new QLabel(ActionType);
    QFont font("serif");
    font.bold();
    font.setPointSize(font.pointSize() + 6);
    title->setFont(font);

    startButton = new QPushButton(QIcon(":/check"), "&" + ActionType);
    stopButton = new QPushButton(QIcon(":/stop"), tr("&Abort"));
    closeButton = new QPushButton(QIcon(":/cross"), tr("&Close"));

    pkList = new QTreeWidget();
    pkList->setHeaderLabel(tr("PACKAGES"));
    pkList->setColumnCount(1);
    pkList->setRootIsDecorated(false);
    notClose = new QCheckBox(tr("Keep this window"));
    isDebug = new QCheckBox(tr("Debug"));
    term = new QTextEdit();
    term->setLineWrapMode(QTextEdit::NoWrap);
    term->setReadOnly(true);

    sb = term->verticalScrollBar();
    sb->setValue(sb->maximum());
    progBar = new QProgressBar();
    progBar->setRange(0, 100);
    dnlData = new QLabel();
    dnlData->setFixedWidth(200);

    butLayout = new QHBoxLayout();
    butLayout->addWidget(stopButton);

    leftLayout = new QVBoxLayout();
    leftLayout->addWidget(pkList);
    leftLayout->addWidget(notClose);
    leftLayout->addWidget(isDebug);
    leftLayout->addWidget(startButton);

    leftLayout->addLayout(butLayout);
    leftLayout->addWidget(closeButton);

    rightLayout = new QVBoxLayout();
    strDebug = new QLabel();
    QString labelStyle = "border: 1.5px solid palette(dark); font-weight: bold;"
                         "padding: 5px; border-radius: 5px;";
    strDebug->setStyleSheet(labelStyle);
    strDebug->setAlignment(Qt::AlignVCenter);
    strDebug->setWordWrap(true);
    strDebug->setTextInteractionFlags(Qt::TextSelectableByMouse);
    strDebug->hide();

    barLayout = new QHBoxLayout();
    barLayout->addWidget(progBar);
    barLayout->addWidget(dnlData);

    rightLayout->addLayout(barLayout);
    rightLayout->addWidget(term, 10);
    rightLayout->addWidget(strDebug);

    hLayout = new QHBoxLayout();
    hLayout->addLayout(leftLayout, 300);
    hLayout->addLayout(rightLayout,900);

    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(title);
    mainLayout->addLayout(hLayout);
    setLayout(mainLayout);

    setFixedSize(QSize(1200, 600));

    connect(isDebug, SIGNAL(clicked()), SLOT(selDebug()));
    connect(startButton, SIGNAL(clicked()), SLOT(startDnl()));
    connect(stopButton, SIGNAL(clicked()), SLOT(stopDnl()));
    connect(closeButton, SIGNAL(clicked()), SLOT(closeButtClicked()));

    connect(pkNetFunc,SIGNAL(valueToMngDnl(int)),SLOT(progress(int)));
    connect(pkNetFunc,SIGNAL(textToMngDnl(QString)),SLOT(dnlLog(QString)));
    connect(pkNetFunc,SIGNAL(valueSpeed(QString)),SLOT(showSP(QString)));
    connect(pkNetFunc,SIGNAL(dnwlFinished()),SLOT(dnlComplete()));

    clearSets();
}

void PkManageDnl::clearSets()
{

    pkFileList.clear();
    pkList->clear();
    notClose->setChecked(true);
    isDebug->setChecked(false);
    term->clear();
    ActionType.clear();
}

void PkManageDnl::manageDnlPkg(const QString &actType, QStringList pkgList)
{
    clearSets();

    ActionType = actType;
    setWidget();
    pkFileList << pkgList;
    loadPkList(pkgList);
    exec();
}

void PkManageDnl::setWidget()
{
    title->setText(ActionType);
    setWindowTitle(PkConstants::getApplicationName() + " - " + ActionType);
    startButton->setText(ActionType);
    progBar->setValue(0);
}

void PkManageDnl::loadPkList(QStringList fileList)
{
    for (int i = 0; i < fileList.size(); i++)
        {
            QFileInfo fileInfo = fileList.at(i);
            QStringList fileColumn;
            fileColumn << fileInfo.fileName();

            pkListItem = new QTreeWidgetItem(pkList, fileColumn);
            pkListItem->setExpanded(true);
            pkListItem->setSelected(false);
            pkListItem->setIcon(0, QIcon(":/slackpkg"));
        }

    startButton->setEnabled(true);
    stopButton->setEnabled(false);
}

void PkManageDnl::startDnl()
{
    pkNetFunc->doDownload(pkFileList);
    startButton->setEnabled(false);
    stopButton->setEnabled(true);
    closeButton->setEnabled(false);

}

void PkManageDnl::stopDnl()
{
    pkNetFunc->abort();

    emit manageDnlFinished();

    startButton->setEnabled(false);
    stopButton->setEnabled(false);
    closeButton->setEnabled(true);
}

void PkManageDnl::progress(int val)
{
    progBar->setValue(val);
}

void PkManageDnl::showSP(QString dataSp)
{
    dnlData->setText(dataSp);
}

void PkManageDnl::dnlComplete()
{
    emit manageDnlFinished();

    if (!notClose->isChecked())
        {
            close();
        }
    startButton->setEnabled(false);
    stopButton->setEnabled(false);
    closeButton->setEnabled(true);
}

void PkManageDnl::dnlLog(QString end)
{
    term->moveCursor (QTextCursor::End);
    term->insertPlainText(end+" \n");
}

void PkManageDnl::selDebug()
{
    if (strDebug->isHidden())
        {
            strDebug->show();
            strDebug->setText(pkFileList.join(" ")+"\n");
        }
    else
        {
            strDebug->clear();
            strDebug->hide();
        }
}

void PkManageDnl::closeButtClicked()
{
    strDebug->clear();
    strDebug->close();
    close();
    accept();
}
