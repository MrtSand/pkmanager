/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKMAINWINDOW_H
#define PKMAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QPushButton>
#include <QFrame>
#include <QSplitter>
#include <QTextBrowser>
#include <QLabel>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QFileSystemWatcher>
#include <QDirIterator>


#include "pktreeview.h"
#include "pkupgtreeview.h"
#include "pksearchline.h"
#include "pkstatusbar.h"
#include "pkinfo.h"
#include "pkfiles.h"
#include "pkmanage.h"
#include "pkmanagednl.h"
#include "pkfindpkg.h"
#include "pkfindfile.h"
#include "pksetup.h"
#include "pkutility.h"
#include "pknetfunc.h"

class PkMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit PkMainWindow(QWidget *parent = 0);
    ~PkMainWindow();


private:
    PkTreeView *instTreeView, *uninstTreeView;
    PkUpgTreeView *upgTreeView;
    PkSearchLine *pkSLine;
    PkStatusBar *pkStatusBar;
    PkInfo *infoText, *pkInfo;
    PkFiles *pkFiles;
    PkManage *pkManage;
    PkManageDnl *pkManageDnl;
    PkFindPkg *pkFindPkg;
    PkFindFile *pkFindFile;
    PkSetup *pkSetup;
    PkUtility *pkUtility;
    PkNetFunc *pkNetFunc;

    QSettings *mySets;

    //Widgets-----------------------------------------------------------
    QGridLayout *mainLayout;
    QPushButton *instButton, *uinstButton;
    QBoxLayout *ltreev, *rtreev, *top, *leftbox, *rightbox, *lbuttons, *rbuttons, *searchBox, *lpkg, *rpkg;
    QFrame *leftpanel, *rightpanel;
    QSplitter *vPan;
    QTabWidget *ltabWidget, *rtabWidget;
    QFrame *searchToolBar, *pkgframe;
    QLabel *searchLabel;
    QLabel *numpkg;
    QToolBar *toolBar ;
    QLabel *statusLabel;
    QSystemTrayIcon *sysTrayIcon;
    QTimer *timer;

    //Buttons -------------------------------------------------------------
    QToolButton *btnOpen, *btnRemove, *btnSrcPkg, *btnSrcFile, *btnReload, *btnDel, *btnUpld;

    //Actions -------------------------------------------------------------
    QAction *openAction;
    QAction *remPkgAction;
    QAction *searchPkgAction;
    QAction *searchFileAction;
    QAction *reloadAction;
    QAction *delPkgAction;
    QAction *openSetup;
    QAction *aboutAction;
    QAction *aboutHelp;
    QAction *aboutQtAction;
    QAction *actionAboutMenu;
    QAction *hideAction;
    QAction *exitAction;

    // menus ---------------------------------------------------------------
    QMenu *fileMenu;
    QMenu *setupMenu;
    QMenu *helpMenu;
    QMenu *sysTrayIconMenu;

    //----------------------------------------------------------------------

    void createForm();
    void createActions();
    void createMenu();
    void createToolBar();

    //-------------------------------------------------------------------
    void showPkgNum();
    void showHwOsInfo();
    void writeSettings();
    void readSettings();

    // various --------------------------------------------------------------
    void loadReposPkgLists();
    void saveLocalPkgsList();
    void setSysTrayIcon();
    void initObj();
    void variusConnect();
    void setRTabs();

    bool isOpen = false;

protected:
    void showEvent(QShowEvent *event);
    void hideEvent(QHideEvent *event);
    void closeEvent(QCloseEvent *event);

    QString itemSelected;

signals:
    void window_loaded();

public slots:
    void callNetSts();

private slots:
    void execSysTrayActivated(QSystemTrayIcon::ActivationReason );

    void ShowPkgInfo(QString item);

    void initCoreApp();
    void loadLocalPkgLists();
    void loadRemTreePkgList();
    void updateInfos();
    void filterTreeView();
    void installPkg();
    void removePkg();
    void deletePkg();

    void searchPkg();
    void searchFile();
    void pkDSetup();

    void stsInt(bool);

    void upld();

    void evPkg(QString evRes);
    void tabLSelected(int selTab);
    void tabRSelected(int selTab);

    void showHelp();
    void about();
    void closeApp();
};

#endif // PKMAINWINDOW_H
