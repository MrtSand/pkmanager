/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKMANAGEDNL_H
#define PKMANAGEDNL_H

#include <QDialog>
#include <QObject>
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QStandardItem>
#include <QCheckBox>
#include <QTextEdit>
#include <QScrollBar>

#include "pknetfunc.h"

#define dnlPKG    "Download Packages"

class PkManageDnl : public QDialog
{
    Q_OBJECT

public:
    explicit PkManageDnl(QWidget *parent = 0);
    ~PkManageDnl();

    void manageDnlPkg(const QString &actType, QStringList pkgList);

private:
    PkNetFunc *pkNetFunc;
    QStringList pkFileList;
    QString ActionType;
    QLabel *title, *strDebug;
    QVBoxLayout *leftLayout, *rightLayout, *mainLayout;
    QHBoxLayout *butLayout, *hLayout,*barLayout;
    QTreeWidget *pkList;
    QTreeWidgetItem *pkListItem;
    QPushButton *startButton,*stopButton,*closeButton;
    QCheckBox *notClose,*isDebug;
    QTextEdit *term;
    QScrollBar *sb;
    QProgressBar *progBar;
    QLabel *dnlData;

    void createForm();
    void clearSets();
    void setWidget();
    void loadPkList(QStringList fileList);

signals:
    void manageDnlFinished();

private slots:
    void progress(int);
    void startDnl();
    void stopDnl();
    void closeButtClicked();
    void selDebug();

    void dnlLog(QString end);
    void showSP(QString dataSp);
    void dnlComplete();
};

#endif // PKMANAGEDNL_H
