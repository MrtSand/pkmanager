/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QMessageBox>
#include <QFileDialog>
#include <QProcess>
#include <QCloseEvent>
#include <QString>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QDebug>

#include "pksetup.h"
#include "pkconstants.h"
#include "pkutility.h"
#include "pknetfunc.h"

PkUtility pkUtility;


PkSetup::PkSetup(QWidget *parent) :
    QDialog(parent)
{
    QString wDir(PkConstants::pathHome());
    QSettings::setPath(QSettings::NativeFormat,QSettings::UserScope,wDir);
    pkSets = new QSettings(QSettings::UserScope, PkConstants::getOrganizationName(), PkConstants::getApplicationName());
}

PkSetup::~PkSetup()
{
    //delete Ui_Dialog();
}

void PkSetup::initDialog()
{
    setupUi(this);
    tbSetup->setCurrentIndex(0);

    connect(btSelDir,SIGNAL(clicked(bool)),this, SLOT(selDirTmp()));
    connect(btOk,SIGNAL(clicked(bool)),this,SLOT(ok_close()));
    connect(btDis,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(btForce,SIGNAL(clicked(bool)),this,SLOT(forceDnl()));
    connect(cbVersion,SIGNAL(stateChanged(int)),this,SLOT(selVersion(int)));

    connect(mirrorList, SIGNAL(clicked(QModelIndex)), this, SLOT(getItemSelected(QModelIndex)));
    connect(swTheme,SIGNAL(valueChanged(int)),this,SLOT(setStyle(int)));
    connect(swUpd,SIGNAL(valueChanged(int)),this,SLOT(mirrorFileDays(int)));
    connect(tbSetup, SIGNAL(currentChanged(int)), this, SLOT(tabSelected(int)));

    setWindowTitle(tr("SetUp"));
    setWindowIcon(QIcon(":/setup"));
    setFixedSize(QSize(640, 480));

    setModal(true);
}

void PkSetup::initSets()
{
    initDialog();
    initBtn();

    loadSettings();

    lbTerminal->setText(PkConstants::bTerm());
    lbTerminal->setStyleSheet("border: 1px solid black; border-radius: 25px;");

    if (leDirTmp->text().isEmpty())
        {
            leDirTmp->setText(PkConstants::tempDir());
        }

    if (leListPkg->text().isEmpty())
        {
            leListPkg->setText(PkConstants::listPkg());
        }

    if(lbMirSelect->text().isEmpty())
        {
            lbMirSelect->setText(PkConstants::defaultMirror());
        }

    if(leVersion->text().isEmpty())
        {
            leVersion->setText(pkUtility.sysArch());
        }
    else if( leVersion->text() == pkUtility.sysArchCurrent())
        {
            cbVersion->setChecked(true);
        }

    setStyle(getStyle());

    if(swUpd->value() == 0)
        {
            swUpd->setValue(PkConstants::daysLeft());
        }

    writeSettings();
}

void PkSetup::initBtn()
{
    btOk->setText(tr("Save"));
    btDis->setText(tr("Discard"));
    btForce->setText(tr("Reload"));
    btForce->setIcon(QIcon(":/reload"));
    btSelDir->setToolTip(tr("Search Dir"));

    leDirTmp->setReadOnly(true);
    leVersion->setReadOnly(true);
    cbVersion->setText("-current");
}

QString PkSetup::selDirTmp()
{
    QString mydirTmp;

    mydirTmp  = QFileDialog::getExistingDirectory( this, tr("Select directory"),"/",
                QFileDialog::DontUseNativeDialog | QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks | QFileDialog::ReadOnly );

    leDirTmp->setText(mydirTmp);

    return mydirTmp;
}

QString PkSetup::getTmpDir()
{
    QString dirTmp;
    dirTmp = getSets("setup","dir").toString();

    return dirTmp + "/";
}

QString PkSetup::getFileListPkg()
{
    QString listPkg;
    listPkg = getSets("setup","listpkg").toString();

    return getDefDir()+listPkg + "_PkgList.xml";
}

QString PkSetup::getRepo()
{
    QString mir;
    mir = getSets("setup","defmir").toString();
    return mir;
}

QString PkSetup::getDefDir()
{
    QFileInfo f(pkSets->fileName());
    QString defDir = f.path();

    return defDir + "/";
}

QString PkSetup::getOsVer()
{
    QString osVer;
    osVer = getSets("setup","version").toString();
    return osVer;
}

int PkSetup::getDays()
{
    int days;
    days = getSets("setup","days").toInt();
    return days;
}

int PkSetup::getStyle()
{
    int style;
    style = getSets("style","theme").toInt();

    return style;
}

void PkSetup::selVersion(int ver)
{
    if(!ver)
        {
            leVersion->setText(pkUtility.sysArch());
        }
    else
        {
            leVersion->setText(pkUtility.sysArchCurrent());
        }
}

///
/// \brief PkSetup::getFullMirrorPath take PACKAGES.TXT from current version
/// \return
///
QString PkSetup::getFullMirrorPath()
{
    QString fullurl;

    fullurl = getRepo()+"/"+pkUtility.sysArchCurrent().toLower()+"/"+ PkConstants::currentMirrFile();
    return fullurl;
}

///
/// \brief PkSetup::getFullMirrorPatchPath take patch/PACKAGES.TXT from release version
/// \return
///
QString PkSetup::getFullMirrorPatchPath()
{
    QString fullurl;

    fullurl = getRepo()+"/"+pkUtility.sysArch().toLower()+"/"+ PkConstants::releaseMirrPatchFile();
    return fullurl;
}

///
/// \brief PkSetup::forceDnl download mirrors sites list
///

void PkSetup::forceDnl()
{
    PkNetFunc *pkNetFunc = new PkNetFunc();

    lbUpldate->setText(tr("Reload !"));

    pkNetFunc->dnlHttpMirrors();
    loadMirrorList();
}

void PkSetup::tabSelected(int selTab)
{
    if(selTab == 1)
        {
            loadMirrorList();
        }
}

void PkSetup::loadMirrorList()
{
    QFile f_mList(getDefDir()+"mirrors");
    QStringList l_mList;

    if(!f_mList.exists())
        {
            QMessageBox msgBox;
            msgBox.setText("File "+f_mList.fileName()+tr(" not found." ));
            msgBox.setInformativeText(tr("Please Force Reload It "));
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();

            return;
        }
    else
        {
            f_mList.open(QIODevice::ReadOnly | QIODevice::Text);

            QTextStream textStream(&f_mList);

            while (!textStream.atEnd())
                {
                    l_mList << textStream.readLine();
                }
            f_mList.close();
        }

    showMirrors(l_mList);
}

void PkSetup::showMirrors(QStringList mirrors)
{
    int it = 0;
    int items = mirrors.count()-1;

    mirrorList->clearContents();
    mirrorList->setRowCount(items);
    mirrorList->setColumnCount(1);
    mirrorList->setHorizontalHeaderLabels(QStringList() << tr(" Country:       Adderss:"));
    mirrorList->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    mirrorList->horizontalHeader()->setStretchLastSection(true);
    mirrorList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mirrorList->setSelectionBehavior( QAbstractItemView::SelectItems );
    mirrorList->setSelectionMode( QAbstractItemView::SingleSelection );

    lbUpldate->setText(mirrors[0]);

    mirrors.removeAt(0);

    for ( const auto& i : mirrors  )
        {
            mirrorList->setItem(it,0,new QTableWidgetItem(i));

            it++;
        }
}

void PkSetup::getItemSelected(QModelIndex item)
{
    QString cellText,selectedMirror;
    if (item.isValid())
        {
            cellText = item.data().toString();
        }

    selectedMirror = cellText.split("\t").takeLast();

    lbMirSelect->setText(selectedMirror);
}

void PkSetup::mirrorFileDays(int days)
{
    if(days == 0 )
        {
            lbDays->setText(tr("Every Start"));
        }
    else if (days == 365)
        {
            lbDays->setText(tr("Every Year"));
        }
    else
        {
            lbDays->setText(tr("%1 Days").arg(days));
        }
}

void PkSetup::setStyle(int val)
{
    switch (val)
        {
        case lightFusion:
            qApp->setStyle(QStyleFactory::create("Fusion"));
            changePalette(Palette::light);
            break;
        case darkFusion:
            qApp->setStyle(QStyleFactory::create("Fusion"));
            changePalette(Palette::dark);
            break;
        default:
            break;
        }
}
#include <QPixmapCache>

void PkSetup::changePalette(bool switchPal)
{
    QPalette Palette;

    if (switchPal)
        {
            Palette.setColor(QPalette::Window, QColor(80,80,80));
            Palette.setColor(QPalette::WindowText, Qt::white);
            Palette.setColor(QPalette::Base, QColor(50,50,50));
            Palette.setColor(QPalette::AlternateBase, QColor(60,60,60));
            Palette.setColor(QPalette::ToolTipBase, QColor(50,50,50));
            Palette.setColor(QPalette::ToolTipText, Qt::white);
            Palette.setColor(QPalette::Text, Qt::white);
            Palette.setColor(QPalette::Button, QColor(55,55,55));
            Palette.setColor(QPalette::ButtonText, Qt::white);
            Palette.setColor(QPalette::BrightText, Qt::red);
            Palette.setColor(QPalette::Link, QColor(42, 130, 218));
            Palette.setColor(QPalette::Highlight, QColor(150, 150, 10));
            Palette.setColor(QPalette::HighlightedText, Qt::black);
        }
    else
        {
            QPixmapCache::clear();
            Palette = style()->QStyle::standardPalette();
        }
    qApp->setPalette(Palette);
    qApp->setStyleSheet(" ");  // only for for reset stylesheets !
}

QVariant PkSetup::getSets(QString getGrp,QString getValue)
{
    QVariant getRes;

    pkSets->beginGroup(getGrp);
    getRes=pkSets->value(getValue);
    pkSets->endGroup();

    return getRes;
}

void PkSetup::setSets(QString setGrp,QString setVar,QVariant setValue)
{
    pkSets->beginGroup(setGrp);
    pkSets->setValue(setVar,setValue);
    pkSets->endGroup();

    pkSets->sync();
}

void PkSetup::loadSettings()
{
    swUpd->setValue(getSets("setup","days").toInt());
    leDirTmp->setText(getSets("setup","dir").toString());
    leVersion->setText(getSets("setup","version").toString());
    leListPkg->setText(getSets("setup","listpkg").toString());
    lbMirSelect->setText(getSets("setup","defmir").toString());

    swTheme->setValue(getSets("style","theme").toInt());
}

void PkSetup::writeSettings()
{
    setSets("setup","days",swUpd->value());
    setSets("setup","dir",leDirTmp->text());
    setSets("setup","version",leVersion->text());
    setSets("setup","listpkg",leListPkg->text());
    setSets("setup","defmir",lbMirSelect->text());

    setSets("style","theme",swTheme->value());
}

void PkSetup::ok_close()
{
    writeSettings();

    done(QDialog::close());
    QTimer::singleShot(100, [this]()
    {
        emit setupFinished();
    });
}
