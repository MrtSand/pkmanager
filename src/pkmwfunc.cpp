/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pkmainwindow.h"
#include "pkconstants.h"
#include "pksetup.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QShowEvent>
#include <QCloseEvent>
#include <QTimer>

void PkMainWindow::initObj()
{
    pkNetFunc = new PkNetFunc();
    pkSetup = new PkSetup(this); //TODO this modal
    pkManage = new PkManage(this);
    pkManageDnl = new PkManageDnl(this);
    pkUtility = new PkUtility();
}

void PkMainWindow::variusConnect()
{
    connect(pkNetFunc,&PkNetFunc::textToStBar, this,[&] (QString msg)
    {
        pkStatusBar->setPbText(msg);
    });

    connect(infoText,&PkInfo::fontSizeChanged,this,[&] (int fontPSize)
    {
        pkInfo->setFont(infoText->font());
        QFont ft(infoText->font());

        pkStatusBar->setPbText(tr("Set Font %1 size %2").arg(ft.family()).arg(fontPSize));
    });

}
void PkMainWindow::setSysTrayIcon()
{
    sysTrayIcon = new QSystemTrayIcon( QIcon(":/pkmanager"), this );
    sysTrayIcon->setObjectName("systemTrayIcon");
    sysTrayIcon->setToolTip(PkConstants::getApplicationName() );
    sysTrayIcon->show();

    sysTrayIconMenu = new QMenu(this);
    actionAboutMenu = new QAction( this );
    actionAboutMenu->setText( tr("About..."));
    sysTrayIconMenu->addAction(aboutAction);
    sysTrayIconMenu->addAction(hideAction);
    sysTrayIconMenu->addAction(exitAction);
    sysTrayIcon->setContextMenu(sysTrayIconMenu );

    connect(actionAboutMenu, SIGNAL(triggered()), this, SLOT(about()));
    connect (sysTrayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
             this,SLOT(execSysTrayActivated(QSystemTrayIcon::ActivationReason)));
}

///
/// \brief PkMainWindow::execSysTrayActivated Active SysTray
/// \param ar
///
void PkMainWindow::execSysTrayActivated ( QSystemTrayIcon::ActivationReason ar )
{
    switch (ar)
        {
        case QSystemTrayIcon::Trigger:
        case QSystemTrayIcon::DoubleClick:
            if ( this->isHidden() )
                {
                    readSettings();
                    if (this->isMinimized()) this->setWindowState(Qt::WindowNoState);
                    this->show();
                }
            else
                {
                    writeSettings();
                    this->hide();
                }
            break;
        default:
            break;
        }
}

///
/// \brief PkMainWindow::loadLocalPkgLists reload packages lists inst. and rem.
///
void PkMainWindow::loadLocalPkgLists()
{
    instTreeView->loadFileList(PkConstants::instListPKG());
    uninstTreeView->loadFileList(PkConstants::uninstListPKG());
    saveLocalPkgsList();  // save packages list

    updateInfos();
}

///
/// \brief PkMainWindow::updateInfos Update system Info on screen
///
void PkMainWindow::updateInfos()
{
    pkSLine->clear();
    pkInfo->clear();
    pkFiles->clear();

    disconnect(rtabWidget,SIGNAL(currentChanged(int)), this, SLOT(tabRSelected(int)));

    rtabWidget->setTabVisible(0,true);
    rtabWidget->setTabVisible(1,false);
    rtabWidget->setTabVisible(2,false);

    showPkgNum();
    showHwOsInfo();
}

///
/// \brief PkMainWindow::saveLocalPkgs save locals packages list
///
void PkMainWindow::saveLocalPkgsList()
{
    instTreeView->saveListPkg();
}

///
/// \brief PkMainWindow::evPkg Sackage selection from package search
/// \param evRes
///
void PkMainWindow::evPkg(QString evRes)
{
    instTreeView->setSelectItem(evRes);
}

///
/// \brief PkMainWindow::filterTreeView  Selecting packages from the searchbar
///
void PkMainWindow::filterTreeView()
{
    pkInfo->clear();
    pkFiles->clear();

    instTreeView->filterData(pkSLine->text());
    upgTreeView->filterData(pkSLine->text());
}

///
/// \brief PkMainWindow::installPkg Launch menu manage  packages (int , uninst , remove , download)
///
void PkMainWindow::installPkg()
{
    if(ltabWidget->currentIndex() == 0)
        {
            QFileInfoList fileListInst;
            QStringList files = QFileDialog::getOpenFileNames(this,
                                tr("Install Packages"), pkSetup->getTmpDir(), tr("Package *.txz *.tgz *.tlz (*.txz *.tgz *.tlz)"));

            if (files.size() != 0)
                {
                    for (int i = 0; i < files.size(); i++)
                        {
                            fileListInst.append(files[i]);
                        }

                    pkManage->managePkg(instPKG,fileListInst);
                }
            else
                {
                    QMessageBox::information(this, tr("Info "), tr("No Packages Selected !"));
                }
        }
    else if (ltabWidget->currentIndex() == 2)
        {
            QStringList files = upgTreeView->getItemsChecked();
            if (files.size() != 0)
                {
                    pkManageDnl->manageDnlPkg(dnlPKG,files);
                }
            else
                {
                    QMessageBox::information(this, tr("Info "), tr("Select Packages !"));
                }
        }
}

///
/// \brief PkMainWindow::removePkg Launch menu unistall packages
///
void PkMainWindow::removePkg()
{
    QStringList files = instTreeView->getItemsChecked();
    QFileInfoList fileListRem;
    QDir::setCurrent(PkConstants::instListPKG());

    if (files.size() != 0)
        {
            for (int i = 0; i < files.size(); i++)
                {
                    fileListRem.append(files[i]);
                }

            pkManage->managePkg(uninstPKG,fileListRem);
        }
    else
        {
            QMessageBox::information(this, tr("Info "), tr("Select Packages First !"));
        }
}

///
/// \brief PkMainWindow::deletePkg Launch menu delete packages
///
void PkMainWindow::deletePkg()
{
    QStringList files = uninstTreeView->getItemsChecked();
    QFileInfoList fileListDel;
    QDir::setCurrent(PkConstants::uninstListPKG());

    if (files.size() != 0)
        {
            for (int i = 0; i < files.size(); i++)
                {
                    fileListDel.append(files[i]);

                }
            pkManage->managePkg(remPKG,fileListDel);
        }
    else
        {
            QMessageBox::information(this, tr("Info "), tr("Select Packages First !"));
        }
}

///
/// \brief PkMainWindow::ShowPkgInfo show the info of the packages in the dedicated window
/// \param item
///
void PkMainWindow::ShowPkgInfo(QString item)
{
    itemSelected = item;
    disconnect(rtabWidget,SIGNAL(currentChanged(int)), this, SLOT(tabRSelected(int)));

    rtabWidget->setTabVisible(0,false);
    rtabWidget->setTabVisible(1,true);
    rtabWidget->setTabVisible(2,true);

    connect(rtabWidget,SIGNAL(currentChanged(int)), this, SLOT(tabRSelected(int)));

    switch (rtabWidget->currentIndex())
        {
        case 0:
        {

        }     break;
        case 1:
        {
            if(!pkInfo->getPkgInfo(item))
                {
                    pkInfo->clear();
                    QMessageBox::information(this, tr("Info "), tr("Unable to read package information %1!\n").arg(item));
                }
        }
        break;
        case 2:
        {
            if (!pkFiles->getPkgFiles(item))
                {
                    pkFiles->clear();
                    QMessageBox::information(this, tr("Info "), tr("Unable to read the list of files %1 !").arg(item));
                }
        }
        break;
        }
}

///
/// \brief PkMainWindow::tabRSelected Recall ShowPkgInfo after tab change
/// \param selTab
///
void PkMainWindow::tabRSelected(int selTab)
{
    Q_UNUSED(selTab);
    ShowPkgInfo(itemSelected);
}

///
/// \brief PkMainWindow::showPkgNum number of packages installed
///
void PkMainWindow::showPkgNum()
{
    QString numPkgValue;
    numPkgValue = tr("Packages Num.: %1").arg(QString::number(instTreeView->pkgCount()));
    numpkg->setText(numPkgValue);
    numpkg->setFixedWidth(numPkgValue.count() * 10);
}

// END CORE

///
/// \brief PkMainWindow::searchPkg Launch menu search package
///
void PkMainWindow::searchPkg()
{
    pkFindPkg = new PkFindPkg();
    pkFindPkg->pkTree = instTreeView;
    pkFindPkg->exec();
}

///
/// \brief PkMainWindow::searchFile Launch menu search file
///
void PkMainWindow::searchFile()
{
    pkFindFile = new PkFindFile();
    pkFindFile->pkBar = pkStatusBar;
    connect(pkFindFile, SIGNAL(pkgContainFile(QString)), this, SLOT(evPkg(QString)));
    pkFindFile->exec();
}

///
/// \brief PkMainWindow::pkDSetup Launch menu setup
///
void PkMainWindow::pkDSetup()
{
    pkSetup->exec();
}

///
/// \brief PkMainWindow::loadReposPkgLists download and save patch and packages list
///
void PkMainWindow::loadReposPkgLists()
{
    pkNetFunc->manageUrlsMirror();

    pkNetFunc->manageMirrorPackageTxt();
}

void PkMainWindow::upld() //NOTE Button for TEST
{
    //for test ....
}

///
/// \brief PkMainWindow::showHwOsInfo  Show Hardware and OsInfo
/// \param isOnline
///
void PkMainWindow::showHwOsInfo()
{
    QString infoCollection = "";
    infoCollection  += "<html><head></head><body>";
    infoCollection  += "<h1>";
    infoCollection  += pkUtility->osVersion();
    infoCollection  += "</h1><hr/>";
    //
    infoCollection  += "<table width='100%' border ='1' cellpadding='5' >";
    infoCollection  += "<tr><td><h3>";
    infoCollection  += tr("Version");
    infoCollection  += "</td><td>";
    infoCollection  += pkUtility->sysArch();
    infoCollection  += "</td></tr>";
    //
    infoCollection  += "<tr><td><h3>";
    infoCollection  += tr("SysInfo");
    infoCollection  += "</td><td>";
    infoCollection  += pkUtility->sysInfo();
    infoCollection  += "</td></tr>";
    //
    infoCollection  += "<tr><td><h3>";
    infoCollection  += tr("Mirror");
    infoCollection  += "</td><td>";
    infoCollection  += pkSetup->getRepo();
    infoCollection  += "</td></tr>";
    //
    infoCollection  += "<tr><td><h3>";
    infoCollection  += tr("Repository");
    infoCollection  += "</td><td>";
    infoCollection  += pkSetup->getOsVer();
    infoCollection  += "</td></tr>";
    //
    infoCollection  += "</body></html>";

    infoText->setText(infoCollection);

    ltabWidget->setTabText(2,pkSetup->getOsVer());
    loadRemTreePkgList();
    //upgTreeView->loadUpgList(pkSetup->getOsVer());
}

///
/// \brief PkMainWindow::loadRemTreePkgList R/Load remote tree-view
///
void PkMainWindow::loadRemTreePkgList()
{
    upgTreeView->unCheckAll();
    upgTreeView->loadUpgList(pkSetup->getOsVer());
}

///
/// \brief PkMainWindow::hideEvent hide applicationin Systray
/// \param event
///
void PkMainWindow::hideEvent(QHideEvent *event)
{
    Q_UNUSED(event);
    hide();

    QMainWindow::hideEvent(event);
}

///
/// \brief PkMainWindow::closeEvent  events when the application closes
/// \param event
///
void PkMainWindow::closeEvent(QCloseEvent *event)
{
    QMainWindow::closeEvent(event);
    Q_UNUSED(event);
    writeSettings();
}

///
/// \brief PkMainWindow::writeSettings Write settings
///
void PkMainWindow::writeSettings()
{
    pkSetup->setSets("mainWindow","geometry",saveGeometry());
    pkSetup->setSets("mainWindow","splitter", vPan->saveState().toBase64());
    pkSetup->setSets("mainWindow","font",infoText->currentFont().toString());
}
void PkMainWindow::readSettings()
{
    restoreGeometry(pkSetup->getSets("mainWindow","geometry").toByteArray());
    vPan->restoreState(QByteArray::fromBase64(pkSetup->getSets("mainWindow","splitter").toByteArray()));
}

void PkMainWindow::closeApp()
{
    QTimer::singleShot(300, [this]()
    {
        close();
        qApp->exit(EXIT_FAILURE);
    });
}

void PkMainWindow::showHelp()  //TODO finire Help
{
    QDialog *helpDiag = new QDialog();
    helpDiag->setModal(true);
    helpDiag->setWindowFlags(Qt::Dialog);
    helpDiag->setFixedSize(600,800);
    QVBoxLayout *vLayout = new QVBoxLayout(helpDiag);

    QTextBrowser *helpText = new QTextBrowser(helpDiag);
    helpText->setReadOnly(true);
    helpText->setFrameShape(QFrame::NoFrame);
    helpText->setFrameShadow(QFrame::Plain);
    helpText->setOpenExternalLinks(true);
    QPushButton *exitBut = new  QPushButton(helpDiag);
    exitBut ->setText(tr("Exit"));

    vLayout->addWidget(helpText);
    vLayout->addWidget(exitBut);

    QString url = "qrc:/help/PkManager_Help_"+QLocale::system().name() + ".html" ;
    helpText->setSource(QUrl(url));

    helpDiag->show();

    connect(exitBut,&QPushButton::clicked, [=]()
    {
        helpDiag->close();
    });
}

void PkMainWindow::about()
{
    QMessageBox::about(this, tr("About"), PkConstants::about());
}
