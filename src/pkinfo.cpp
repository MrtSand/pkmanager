/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pkinfo.h"
#include "pksetup.h"

#include <QDesktopServices>
#include <QDebug>

PkInfo::PkInfo(QWidget *parent) :
    QTextBrowser(parent)
{
    PkSetup pkSetup;
    setFont(pkSetup.getSets("mainWindow","font").value<QFont>());

    setAlignment(Qt::AlignCenter);

    setReadOnly(true);
    setOpenLinks(false);

    connect(this, SIGNAL(highlighted(QString)),this,SLOT(overUrl(QString)));
    connect(this, SIGNAL(anchorClicked(QUrl)), this,SLOT(openUrl(const QUrl&)));
}


PkInfo::~PkInfo()
{}

bool PkInfo::getPkgInfo(QString dataRow)
{
    if (!dataRow.isEmpty())
        {
            QMap<QString, QString> info ;
            if (pkInterface->infoPkg(info, dataRow))
                {
                    formatPkgInfo(info);
                    return true;
                }
            else
                {
                    return false;
                }
        }
    return false;
}

void PkInfo::formatPkgInfo(QMap<QString, QString> infoPkg)
{
    if (!infoPkg.empty())
        {
            stmp = "";
            httpurl = "";

            if (!infoPkg["http"].isEmpty())
                {
                    httpurl = "<a href=" + infoPkg["http"] + ">" + infoPkg["http"] + "</a>";
                }
            else
                {
                    httpurl = tr(" URL not found");
                }

            if (!infoPkg["name"].isEmpty())
                {
                    stmp += "<html><head></head><body>";
                    stmp += "<h1>";
                    stmp += "  " + infoPkg["name"];
                    stmp += "</h1><hr/>";

                    stmp += "<table width='100%' border ='0' cellpadding='5' >";
                    stmp += "<tr><td>";
                    stmp += tr("<h4>Summary:</h4>");
                    stmp += "</td><td>";
                    stmp += infoPkg["summary"];
                    stmp += "</td></tr><tr><td>";
                    stmp += tr("<h4>Version:</h4>");
                    stmp += "</td><td>";
                    stmp += infoPkg["version"];
                    stmp += "</td></tr><tr><td>";
                    stmp += tr("<h4>Architecture:</h4>");
                    stmp += "</td><td>";
                    stmp += infoPkg["arch"];
                    stmp += "</td></tr><tr><td>";
                    stmp += tr("<h4>Group:</h4>");
                    stmp += "</td><td>";
                    stmp += infoPkg["group"];
                    stmp += "</td></tr><tr><td>";
                    stmp += tr("<h4>Package Size:</h4>");
                    stmp += "</td><td>";;
                    stmp += infoPkg["comp-size"];
                    stmp += "</td></tr><tr><td>";
                    stmp += tr("<h4>Files Size:</h4>");
                    stmp += "</td><td>";
                    stmp += infoPkg["uncomp-size"];
                    stmp += "</td></tr><tr><td>";
                    stmp += tr("<h4>Description:</h4>");
                    stmp += "</td><td>";
                    stmp += infoPkg["description"];
                    stmp += "</td></tr><tr><td>";
                    stmp += "<h4>URL:</h4>";
                    stmp += "</td><td>";
                    stmp += httpurl;
                    stmp += "</td></tr>";
                    stmp += "</table><hr/>";
                    stmp += "</body></html>";

                    setText(stmp);

                    pkBar->setPbText(tr("Open Info of ")+ infoPkg["name"]);
                }
            else
                {
                    setText(tr("Package 'info' empty !"));
                }
        }
    else
        {
            setText(tr("No Package recognize"));
        }
}

void PkInfo::overUrl(QString strurl)
{
    if(!strurl.isNull())
        pkBar->setPbText(tr("Click to Open URL ")+ strurl);
}

void PkInfo::openUrl(QUrl url)
{
    pkBar->setPbText(tr("Open URL ")+ url.toString());
    QDesktopServices::openUrl(url);
}
#include <iostream>
void PkInfo::wheelEvent(QWheelEvent *event)
{
    QTextBrowser::wheelEvent(event);

    if(event->angleDelta().y() > 0) // up Wheel
        {
            emit fontSizeChanged(currentFont().pointSize());
        }
    else if(event->angleDelta().y() < 0) //down Wheel
        {
            emit fontSizeChanged(currentFont().pointSize());
        }
}

