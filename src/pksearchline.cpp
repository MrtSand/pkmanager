/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QStyle>

#include "pksearchline.h"

PkSearchLine::PkSearchLine(QWidget *parent) :
    QLineEdit(parent)
{
    clearButton = new QToolButton(this);
    QPixmap pixmap(":/rclear");
    clearButton->setIcon(QIcon(pixmap));
    clearButton->setIconSize(pixmap.size());
    clearButton->setCursor(Qt::ArrowCursor);
    clearButton->setStyleSheet("QToolButton { border: none; padding: 0px; }");
    clearButton->setToolTip(tr("Clears the text"));
    clearButton->hide();
    //setText(QString("Search Package .."));

    connect(clearButton, SIGNAL(clicked()), this, SLOT(clearText()));
    connect(this,SIGNAL(textChanged(const QString&)), this,SLOT(updateSearchLine(const QString&)));

    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    setStyleSheet(QString("pkSearchLine { padding-right: %1px; } ").arg(clearButton->sizeHint().width() + frameWidth + 1));
    QSize msz = minimumSizeHint();
    setMinimumSize(qMax(msz.width(), clearButton->sizeHint().height() + frameWidth * 2 + 2),
                   qMax(msz.height(), clearButton->sizeHint().height() + frameWidth * 2 + 2));
}

PkSearchLine::~PkSearchLine()
{}

void PkSearchLine::resizeEvent(QResizeEvent *)
{
    QSize sz = clearButton->sizeHint();
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    clearButton->move(rect().right() - frameWidth - sz.width(),
                      (rect().bottom() + 2 - sz.height()) / 2);
}

void PkSearchLine::updateSearchLine(const QString &text)
{
    clearButton->setVisible(!text.isEmpty());
}

void PkSearchLine::clearText()
{
    clear();
    emit searchLineClean();
}


