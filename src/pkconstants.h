/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKCONSTANTS
#define PKCONSTANTS

#include <QString>
#include <QDir>
#include <QTextStream>
#include <QFile>
#include <QSettings>
#include <QProcess>
#include <QMessageBox>


class PkConstants
{

private:

public:

    static QString getApplicationName()
    {
        return "PkManager";
    }
    static QString getOrganizationName()
    {
        return "PkManager-Utility";
    }

    static QString getVerApp()
    {
        return " 1.01";
    }

    static QString tempDir()
    {
        return "/tmp";
    }

    static QString listPkg()
    {
        return "PkManager";
    }

    //--#define---------------------------------------------------------------

    static QString pathHome()
    {
        return QDir::homePath();
    }

    static QString instListPKG()
    {
        return "/var/log/packages/";
    }

    static QString uninstListPKG()
    {
        return "/var/log/removed_packages/";
    }

    static QString dataFormated()
    {
        return "dd MMM yy";
    }

    static int daysLeft()
    {
        return 30;
    }

    static int updateDays()
    {
        return 1;
    }

    //--#remote---------------------------------------------------------------

    static QString mirrorSite()
    {
        return "https://mirrors.slackware.com/mirrorlist/";
    }

    static QString defaultMirror()
    {
        return "https://mirrors.slackware.com/slackware/";
    }

    static QString urlMirrFile()
    {
        return "mirrors";
    }

    static QString currentMirrFile()
    {
        return "PACKAGES.TXT";
    }

    static QString releaseMirrPatchFile()
    {
        return "patches/PACKAGES.TXT";
    }

    static QString localPatchFile()
    {
        return "patch_PACKAGES.xml";
    }

    static QString localCurrentFile()
    {
        return "current_PACKAGES.xml";
    }

    //--#terminal------------------------------------------------------------

    static QString bTerm()
    {
        return "/usr/bin/xterm";
    }

    //--#theme---------------------------------------------------------------

    static QString defaultTheme()
    {
        return "lightFusion";
    }

    //--#commands---------------------------------------------------------------

    static QString suCommand()
    {
        return "/usr/bin/pkexec ";
    }

    static QString instCommand()
    {
        return "/sbin/installpkg ";
    }

    static QString uninstCommand()
    {
        return "/sbin/removepkg ";
    }

    static QString updateCommand()
    {
        return "/sbin/upgradepkg --install-new ";
    }

    static QString delCommand()
    {
        return "rm -v ";
    }

    //--#about---------------------------------------------------------------

    static QString msgLock()
    {
        return QObject::tr("The application is already running.\n"
                           "Allowed to run only one instance of the application.");
    }

    static QString about()
    {
        return QObject::tr("PkManager:  Slackware Package Tool.\n "
                          ) + getApplicationName() + getVerApp();
    }
};

#endif // PKCONSTANTS
