/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QMessageBox>
#include <QMenu>
#include <QFile>
#include <QAction>
#include <QInputDialog>
#include <QLineEdit>

#include "pkfiles.h"
#include "pkconstants.h"


PkFiles::PkFiles(QWidget *parent) :
    QListWidget(parent)
{
    setSortingEnabled(true);
    setSpacing(1);
    sortItems(Qt::AscendingOrder);
    setContextMenuPolicy(Qt::CustomContextMenu);


    pkUtility = new PkUtility();

    connect(this,SIGNAL(itemActivated(QListWidgetItem*)),SLOT(lListMenu(QListWidgetItem*)));
    connect(this,SIGNAL(customContextMenuRequested(const QPoint &)),SLOT(rListMenu(const QPoint&)));
}

PkFiles::~PkFiles()
{}

bool PkFiles::getPkgFiles(QString dataRow)
{
    pkBar->setPbText(tr("List Files of %1").arg(dataRow));

    QStringList fileList;
    if (pkInterface->infoFileList(fileList, dataRow))
        {
            formatPkgFiles(fileList);
            return true;
        }
    else
        {
            return false;
        }
}

void PkFiles::formatPkgFiles(QStringList fileList)
{
    QFile file;
    QIcon icon;
    QString itemc;

    clear();
    scrollToTop();

    for (int i = 0; i < fileList.size(); i++)
        {
            if (file.exists(fileList[i]))
                {
                    icon.addFile(":/check");
                }
            else
                {
                    icon.addFile(":/cross");
                }

            fileItem = new QListWidgetItem(icon, fileList[i], this);

            itemc = fileItem->text();

            if (itemc.section('/', 1, 2) == "usr/bin" || itemc.section('/', 1, 2) == "usr/sbin")
                {
                    fileItem->setForeground(Qt::magenta);
                }
            else if (itemc.section('/', 0, 1) == "/usr" || itemc.section('/', 0, 1) == "/opt")
                {
                    fileItem->setForeground(Qt::darkGreen);
                }
            else if (itemc.section('/', 0, 1) == "/etc" || itemc.section('/', 0, 1) == "/dev")
                {
                    fileItem->setForeground(Qt::darkGreen);
                }
        }
}

void PkFiles::lListMenu(QListWidgetItem *item)
{
    PkUtility pkUtility;
    QFile file;

    QFileInfo filepath(item->text());

    QString fileName = item->text();

    if (item)
        {
            if(file.exists(fileName))
                {
                    if (!pkUtility.isExec(fileName))
                        {
                            QMessageBox::information(this,
                                                     tr("Open File "),
                                                     tr("File %1 is not a program! \nUse right click to open Terminal or FileBrowser").arg(fileName));
                        }
                    else
                        {
                            if (QMessageBox::Yes == QMessageBox(QMessageBox::Question,
                                                                tr("Run Program File"),
                                                                tr("Try to Run File %1 \n in %2 ").arg(fileName).arg(PkConstants::bTerm()),
                                                                QMessageBox::Yes|QMessageBox::No).exec())
                                {
                                    pkUtility.cmdTerm(NULL,filepath.fileName());
                                }
                        }
                }
            else
                {
                    QMessageBox::information(this,
                                             tr("Open Program File "),
                                             tr("File %1 not exist !").arg(fileName));
                }


        }
}

void PkFiles::rListMenu(const QPoint &_pos)
{
    pos = _pos;
    QMenu Menu;
    QFile file;
    QAction *opTerm = new QAction(QIcon(":/term"), tr("Open terminal here"), this);
    QAction *opDir = new QAction(QIcon(":/folder"), tr("Open directory here"), this);

    connect(opTerm, SIGNAL(triggered()), this, SLOT(openTerminal()));
    connect(opDir, SIGNAL(triggered()), this, SLOT(openDirectory()));

    Menu.addAction(opTerm);
    Menu.addAction(opDir);

    QListWidgetItem *item = itemAt(pos);

    if (item)
        {
            if (file.exists(item->text()))
                {
                    Menu.exec(mapToGlobal(pos));
                }
            else
                {
                    QMessageBox::information(this, tr("Open File "), tr("File %1 not exist !").arg(item->text()));
                }
        }
}

void PkFiles::openTerminal()
{
    QListWidgetItem *item = itemAt(pos);
    QFileInfo filepath(item->text());

    pkUtility->cmdTerm(filepath.path(), NULL);
}

void PkFiles::openDirectory()
{
    QListWidgetItem *item = itemAt(pos);
    QFileInfo finfo(item->text());
    QString dirName = finfo.path();

    QProcess().startDetached("xdg-open", QStringList(dirName));
}

