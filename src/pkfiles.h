/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKFILES_H
#define PKFILES_H

#include <QListWidget>
#include <QProcess>
#include <QApplication>

#include "pkinterface.h"
#include "pkutility.h"
#include "pkstatusbar.h"

class PkFiles : public QListWidget
{
    Q_OBJECT

public:
    explicit PkFiles(QWidget *parent = 0);
    ~PkFiles();
    PkStatusBar *pkBar;
    bool getPkgFiles(QString dataRow);

private:
    QPoint pos;

    PkInterface *pkInterface;
    PkUtility *pkUtility;
    QListWidgetItem *fileItem;
    QString  pkgName;

    void formatPkgFiles(QStringList fileList);

signals:

public slots:

private slots:
    void lListMenu(QListWidgetItem *item);
    void rListMenu(const QPoint &);
    void openDirectory();
    void openTerminal();

};

#endif // PKFILES_H
