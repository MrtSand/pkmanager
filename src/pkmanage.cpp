/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pkmanage.h"
#include "pkconstants.h"

#include <iostream>
#include <stdio.h>
#include <string>
#include <unistd.h>

#include <QScrollBar>

#define WARNING "WARNING: Unique directory "

PkManage::PkManage(QWidget *parent) : QDialog(parent)
{
    createForm();

    pkTerm = new QProcess(this);
    pkTerm->setProcessChannelMode(QProcess::MergedChannels);

    setModal(true);
}

PkManage::~PkManage()
{}

void PkManage::createForm()
{
    setWindowIcon(QIcon(":/manage"));
    title = new QLabel(ActionType);
    QFont font("serif");
    font.bold();
    font.setPointSize(font.pointSize() + 6);
    title->setFont(font);

    actionButton = new QPushButton(QIcon(":/check"), "&" + ActionType);
    closeButton = new QPushButton(QIcon(":/cross"), tr("&Close"));
    stopProcButton = new QPushButton(QIcon(":/stop"), tr("&Stop"));
    stopProcButton->setToolTip("Use at Own Risk !!");
    stopProcButton->setFixedWidth(70);
    upgradeSel = new QCheckBox(tr("Upgrade Package"));
    testInst = new QCheckBox(tr("Test (warn)"));
    verDel = new QCheckBox(tr("Forcefully "));
    notClose = new QCheckBox(tr("Keep this window"));
    isDebug = new QCheckBox(tr("Debug"));
    pkList = new QTreeWidget();
    pkList->setHeaderLabel(tr("PACKAGES"));
    pkList->setColumnCount(1);
    pkList->setRootIsDecorated(false);
    term = new QTextEdit();
    term->setLineWrapMode(QTextEdit::NoWrap);
    term->setReadOnly(true);

    QScrollBar *sb = term->verticalScrollBar();
    sb->setValue(sb->maximum());

    butLayout = new QHBoxLayout();
    butLayout->addWidget(stopProcButton);
    butLayout->addWidget(closeButton);

    leftLayout = new QVBoxLayout();
    leftLayout->addWidget(pkList);
    leftLayout->addWidget(upgradeSel);
    leftLayout->addWidget(testInst);
    leftLayout->addWidget(verDel);
    leftLayout->addWidget(notClose);
    leftLayout->addWidget(isDebug);
    leftLayout->addWidget(actionButton);

    leftLayout->addLayout(butLayout);

    rightLayout = new QVBoxLayout();
    strDebug = new QLabel();
    QString labelStyle = "border: 1.5px solid palette(dark); font-weight: bold;"
                         "padding: 5px; border-radius: 5px;";
    strDebug->setStyleSheet(labelStyle);
    strDebug->setAlignment(Qt::AlignVCenter);
    strDebug->setWordWrap(true);
    strDebug->setTextInteractionFlags(Qt::TextSelectableByMouse);
    strDebug->hide();
    rightLayout->addWidget(term, 10);
    rightLayout->addWidget(strDebug);

    hLayout = new QHBoxLayout();
    hLayout->addLayout(leftLayout, 300);
    hLayout->addLayout(rightLayout, 900);

    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(title);
    mainLayout->addLayout(hLayout);
    setLayout(mainLayout);

    setFixedSize(QSize(1200, 600));

    connect(upgradeSel, SIGNAL(clicked()), SLOT(selUpgrade()));
    connect(testInst, SIGNAL(clicked()), SLOT(selTestInst()));
    connect(verDel, SIGNAL(clicked()), SLOT(selVerDel()));
    connect(isDebug, SIGNAL(clicked()), SLOT(selDebug()));
    connect(actionButton, SIGNAL(clicked()), SLOT(pkActionButtClicked()));
    connect(stopProcButton, SIGNAL(clicked()), SLOT(stopProcess()));
    connect(closeButton, SIGNAL(clicked()), SLOT(cancelButtClicked()));

    clearSets();
}

void PkManage::clearSets()
{
    pkFileList.clear();
    pkList->clear();
    term->clear();
    ActionType.clear();
    strDebug->clear();
    upgradeSel->setChecked(false);
    testInst->setChecked(false);
    testInst->setEnabled(true);
    notClose->setChecked(true);
    isDebug->setChecked(false);
    verDel->setChecked(false);
}

void PkManage::managePkg(const QString &actType, QFileInfoList pkgList)
{
    clearSets();

    ActionType = actType;
    setWidget();
    pkFileList << pkgList;
    loadPkList(pkgList);
    exec();
}

void PkManage::setWidget()
{
    title->setText(ActionType);
    setWindowTitle(PkConstants::getApplicationName() + " - " + ActionType);
    actionButton->setText(ActionType);

    if (ActionType == instPKG)
        {
            upgradeSel->show();
            verDel->hide();
        }
    else if (ActionType == uninstPKG)
        {
            upgradeSel->hide();
            verDel->hide();
        }
    else if (ActionType == remPKG)
        {
            testInst->hide();
            upgradeSel->hide();
            verDel->show();
        }
}

void PkManage::loadPkList(QFileInfoList fileList)
{
    for (int i = 0; i < fileList.size(); i++)
        {
            QFileInfo fileInfo = fileList.at(i);
            QStringList fileColumn;
            fileColumn << fileInfo.fileName();

            pkListItem = new QTreeWidgetItem(pkList, fileColumn);
            pkListItem->setExpanded(true);
            pkListItem->setSelected(false);
            pkListItem->setIcon(0, QIcon(":/slackpkg"));
        }

    actionButton->setEnabled(true);
    stopProcButton->setEnabled(false);
}

void PkManage::selUpgrade()
{
    if (testInst->isChecked())
        {
            testInst->setEnabled(false);
        }
    else
        {
            testInst->setEnabled(true);
        }
    strDebug->setText(textDebug(cmdString()));
}

void PkManage::selTestInst()
{
    strDebug->setText(textDebug(cmdString()));
}

void PkManage::selVerDel()
{
    strDebug->setText(textDebug(cmdString()));
}

void PkManage::selDebug()
{
    if (strDebug->isHidden())
        {
            strDebug->show();
            strDebug->setText(textDebug(cmdString()));
        }
    else
        {
            strDebug->clear();
            strDebug->hide();
        }
}

QString PkManage::cmdString()
{
    QString cmd, cmdMode;
    MyStringList listPkgName;
    QFileInfo fileInfo;
    int uid;

    for (int i = 0; i < pkFileList.size(); i++)
        {
            fileInfo = pkFileList.at(i);
            listPkgName.append(fileInfo.absoluteFilePath());
        }
    if (ActionType == instPKG) // Inst or upgrade pkg
        {
            if (testInst->isChecked()) // Is test inst
                {
                    if (upgradeSel->isChecked())
                        {
                            cmdMode = PkConstants::updateCommand();
                            testInst->setChecked(false);
                        }
                    else
                        {
                            cmdMode = PkConstants::instCommand() + "--warn ";
                        }
                }
            else   // not test inst
                {
                    if (upgradeSel->isChecked())
                        {
                            cmdMode = PkConstants::updateCommand();
                        }
                    else
                        {
                            cmdMode = PkConstants::instCommand();
                        }
                }
        }
    else if (ActionType == uninstPKG)   // Remove pkg
        {
            if (testInst->isChecked())
                {
                    cmdMode = PkConstants::uninstCommand() + "-warn ";
                }
            else
                {
                    cmdMode = PkConstants::uninstCommand();
                }
        }
    else if (ActionType == remPKG)   // Delete pkg
        {
            if (verDel->isChecked())
                {
                    cmdMode = PkConstants::delCommand() + "-f ";
                }
            else
                {
                    cmdMode = PkConstants::delCommand();
                }
        }

    uid = getuid();

    if (uid != 0)
        {
            cmd = PkConstants::suCommand() + cmdMode + (QString)listPkgName + "\n";
        }
    else
        {
            cmd = cmdMode + (QString)listPkgName + "\n";
        }

    return cmd;
}

QString PkManage::textDebug(QString text)
{
    return text.remove(PkConstants::suCommand());
}

void PkManage::pkActionButtClicked()
{
    execCmdStr(cmdString());
}

void PkManage::execCmdStr(QString cmd)
{
    term->clear();

    connect(pkTerm, SIGNAL(started()), this, SLOT(isStarted()));
    connect(pkTerm, SIGNAL(readyReadStandardOutput()), this, SLOT(showOutput()));
    connect(pkTerm, SIGNAL(finished(int)), this, SLOT(actionComplete(int)));

    pkTerm->start(cmd.toUtf8());
}

void PkManage::isStarted()
{
    term->insertPlainText(tr("Start %1 with process ID: %2\n").arg(ActionType).arg(pkTerm->processId()));

    closeButton->setEnabled(false);
    stopProcButton->setEnabled(true);
    actionButton->setEnabled(false);
}

void PkManage::showOutput()
{
    QString str = pkTerm->readAllStandardOutput();

    if (str.contains(WARNING))
        {
            term->setTextColor(QColor(Qt::magenta));
        }
    term->insertPlainText(str);
    term->ensureCursorVisible();
}

void PkManage::actionComplete(int res)
{
    actionButton->setEnabled(false);
    stopProcButton->setEnabled(false);
    closeButton->setEnabled(true);

    if (!pkTerm->exitCode())
        {
            term->append("Completed Successfully : RESULT= " + QString::number(res));
            disconnect(pkTerm,0,0,0);
            pkTerm->close();
        }
    else
        {
            term->append("Error : RESULT= " + QString::number(res));
            disconnect(pkTerm,0,0,0);
            pkTerm->close();
        }

    term->ensureCursorVisible();

    if (res == 0)
        {
            emit manageFinished();
        }

    if (!notClose->isChecked())
        {
            close();
        }
}

void PkManage::stopProcess()
{
    term->insertPlainText(tr("WARNING: STOP !!\n"));
    if(pkTerm->processId())
        {
            term->insertPlainText(tr("%1 with process ID: %2 as stopped !!\n").arg(ActionType).arg(pkTerm->processId()));
            disconnect(pkTerm,0,0,0);
            pkTerm->close();

            if(!pkTerm->isOpen())
                {

                    stopProcButton->setEnabled(false);
                    closeButton->setEnabled(true);

                    emit manageFinished();
                }
        }
}

void PkManage::cancelButtClicked()
{
    strDebug->close();
    close();
    accept();
}
