/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/


#include "pktreeview.h"
#include "pkconstants.h"
#include "pksetup.h"

#include <QHeaderView>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QScrollBar>
#include <QTimer>

#include <QDebug>


PkTreeView::PkTreeView(QWidget *parent) :
    QTreeView(parent)
{
    model = new QStandardItemModel();
    proxyModel  = new SearchTreeProxyModel;

    createTree();
    headerActions();
}

PkTreeView::~PkTreeView()
{}

//PUBLICS
void PkTreeView::loadFileList(const QString &parList)
{
    filterData(QString());
    model->removeRows(0, model->rowCount());
    populateTree(parList);

    setColumnHidden(3, true);
    unCheckAll();
    resizeCol();
    clearFocus();
}

void PkTreeView::filterData(const QString &filter)
{
    proxyModel->setDynamicSortFilter(true);
    proxyModel->setSourceModel(model);
    proxyModel->setFilterKeyColumn(0);

    setModel(proxyModel);
    expandAll();

    proxyModel->setFilterWildcard(filter);
}

QStringList PkTreeView::getItemsChecked()
{
    QStringList pkgChecked;

    for (int i = 0; i < model->rowCount(); ++i)
        {
            if (model->item(i, 2)->checkState() == Qt::Checked)
                {
                    pkgChecked << model->item(i, 0)->text();
                }
        }

    return pkgChecked;
}

QStringList PkTreeView::getItemSearch(QString &name, bool b_substr, bool b_showp)
{
    QStringList resItem;
    QList<QStandardItem *> itFind;

    if (b_substr)
        {
            itFind =  model->findItems(name, Qt::MatchContains, 0);
        }
    else
        {
            itFind =  model->findItems(name, Qt::MatchFixedString, 0);
        }
    if (b_showp && itFind.count())
        {
            filterData(name);
        }

    for (int i = 0; i < itFind.size(); ++i)
        {
            resItem << itFind.at(i)->text();

        }
    return resItem;
}

void PkTreeView::setSelectItem(const QString &itemToSel)
{
    for (int i = 0; i < model->rowCount(); ++i)
        {
            if (model->item(i, 0)->text() == itemToSel)
                {

                    selectionModel()->setCurrentIndex(model->item(i, 0)->index(), QItemSelectionModel::SelectCurrent);
                    selectionModel()->select(model->item(i, 0)->index(), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);

                    verticalScrollBar()->setValue(model->item(i, 0)->row());
                }
        }
}

void PkTreeView::resizeCol()
{
    int nColums = model->columnCount();

    for (int col = 0 ; col < nColums ; col ++)
        {
            resizeColumnToContents(col);
        }
}

//PRIVATE
void PkTreeView::createTree()
{
    fData = PkConstants::dataFormated();
    PkHeader *pkHeader = new PkHeader(Qt::Horizontal);
    setHeader(pkHeader);

    model->setHorizontalHeaderItem(0, new QStandardItem(tr("Package")));
    model->setHorizontalHeaderItem(1, new QStandardItem(tr("Date")));
    model->setHorizontalHeaderItem(2, new QStandardItem(tr("Mark")));
    model->setHorizontalHeaderItem(3, new QStandardItem(tr("Date_Hide")));

    setAllColumnsShowFocus(true);
    setAlternatingRowColors(true);
    setRootIsDecorated(false);

    setStyleSheet(
        "PkTreeView::indicator:unchecked {image: url(:/unsel);}"
        "PkTreeView::indicator:checked {image: url(:/check);}");

    connect(pkHeader, SIGNAL(customSignal(int)), SLOT(HeaderMenu(int)));
}

void PkTreeView::populateTree(QString repoPackages)
{
    count = 0;

    pkBar->setProgValue(0);

    QFileInfoList dirList = pkInterface->dirFileList(repoPackages) ;
    int countPkg = dirList.count();

    pkBar->setPbText(tr("Load Packages List..from ") + repoPackages);

    for (QFileInfoList::Iterator it = dirList.begin(); it != dirList.end(); it++)
        {
            QFileInfo &fi  = *it;
            pkFileName = fi.fileName();
            pkFileDateTime = fi.metadataChangeTime();

            pkg_Item = new QStandardItem(QIcon(":/slackpkg"),pkFileName);   //name
            pkg_Item->setEditable(false);
            pkg_Item->setDragEnabled(false);
            pkg_Item->setDropEnabled(false);
            //pkgItem->setCheckable(true);
            model->setItem(count, 0, pkg_Item);

            d_Item = new QStandardItem(pkFileDateTime.toString(fData));     //date
            d_Item->setEditable(false);
            d_Item->setDragEnabled(false);
            d_Item->setDropEnabled(false);
            model->setItem(count, 1, d_Item);

            ck_Item = new QStandardItem();                                  //check
            ck_Item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
            ck_Item->setData(Qt::Unchecked, Qt::CheckStateRole);
            ck_Item->setCheckable(true);
            ck_Item->setSelectable(true);
            ck_Item->setEditable(false);
            ck_Item->setDragEnabled(false);
            ck_Item->setDropEnabled(false);
            ck_Item->setSizeHint(QSize(20, 22));
            model->setItem(count, 2, ck_Item);

            dataInst_Hide = new QStandardItem(QString::number(pkFileDateTime.toMSecsSinceEpoch())); // date hide
            model->setItem(count, 3, dataInst_Hide);

            count++;

            int n = (count * 100) / countPkg;
            if (!(n % 5))
                {
                    pkBar->setProgValue(n);
                }
        }
}

int PkTreeView::pkgCount()
{
    return model->rowCount();
}

///
/// \brief PkTreeView::saveListPkg save list packages installed
///
void PkTreeView::saveListPkg()
{
    PkSetup pkSetup;
    QStringList pkg_List ;
    QFile file(pkSetup.getFileListPkg());

    for( int row = 0; row < model->rowCount(); row++ )
        {
            pkg_List << model->item(row,0)->text()+"\n";
        }

    // Open or create the file if it does not exist
    if(file.open( QIODevice::WriteOnly ))

        {
            QXmlStreamWriter xmlWriter(&file);
            xmlWriter.setAutoFormatting(true);  // Set the auto-formatting text
            xmlWriter.writeStartDocument();     // run record in a document
            xmlWriter.writeStartElement("Packages");


            QStringList::Iterator it = nullptr;

            for (it = pkg_List.begin(); it !=  pkg_List.end(); ++it)
                {
                    QString current = *it;

                    xmlWriter.writeStartElement("name");
                    xmlWriter.writeCharacters(current.simplified());   // Write the date
                    xmlWriter.writeEndElement();

                }
            xmlWriter.writeEndElement();
            xmlWriter.writeEndDocument();
            file.flush();
            file.close();
        }
}

///
/// \brief PkTreeView::selectionChanged read file info  on mouseclick or arrow keys
/// \param selected
/// \param deselected
///
void PkTreeView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    QTreeView::selectionChanged(selected,deselected);

    if (!selected.empty())
        {
            emit itemSel(selected.indexes().first().data().toString());
        }
}

///
/// \brief PkTreeView::headerActions
///
void PkTreeView::headerActions()
{
    resizeTree = new QAction(QIcon(":/unsel"), tr("Resize "),  this);
    resizeTree->setToolTip(tr("Resize by Content"));
    menu0.addAction(resizeTree);

    //--------------------------------

    sortingUp = new QAction(QIcon(":/unsel"), tr("New First"),  this);
    sortingDw = new QAction(QIcon(":/unsel"), tr("Old First"),  this);
    menu1.addAction(sortingUp);
    menu1.addAction(sortingDw);

    //--------------------------------

    uncheck = new QAction(QIcon(":/unsel"), tr("Uncheck All"), this);
    uncheck->setObjectName(tr("uncheck"));
    menu2.addAction(uncheck);
}

///
/// \brief PkTreeView::HeaderMenu
/// \param section
///
void PkTreeView::HeaderMenu(int section)
{
    switch (section)
        {
        case 0:
        {
            connect(resizeTree, SIGNAL(triggered()), this, SLOT(resizeCol()));
            menu0.popup(QCursor::pos());
        }
        break;
        case 1 :
        {
            connect(sortingUp, SIGNAL(triggered()), this, SLOT(sortDataUp()));
            connect(sortingDw, SIGNAL(triggered()), this, SLOT(sortDataDw()));
            menu1.popup(QCursor::pos());
        }
        break;
        case 2 :
        {
            connect(uncheck, SIGNAL(triggered()), this, SLOT(unCheckAll()));
            menu2.popup(QCursor::pos());
        }
        break;
        default:
            break;
        }
}

///
/// \brief PkTreeView::sortDataUp
///
void PkTreeView::sortDataUp()
{
    model->sort(3,Qt::DescendingOrder);
}

///
/// \brief PkTreeView::sortDataDw
///
void PkTreeView::sortDataDw()
{
    model->sort(3,Qt::AscendingOrder);
}

///
/// \brief PkTreeView::unCheckAll
///
void PkTreeView::unCheckAll()
{
    for (int i = 0; i < model->rowCount(); ++i)
        {
            if (model->item(i, 2)->checkState() == Qt::Checked)
                {
                    model->item(i, 2)->setCheckState(Qt::Unchecked);
                }
        }
}

///
/// \brief PkTreeView::keyPressEvent
/// \param event
///
void PkTreeView::keyPressEvent(QKeyEvent* event) //Not work if row is selected by mark coloum !
{
    QTreeView::keyPressEvent(event);

    QModelIndex indexFilter = proxyModel->mapToSource(currentIndex());

    if(indexFilter.isValid() && event->key() == Qt::Key_Space)
        {
            int itemRow  = indexFilter.row();

            if (model->item(itemRow, 2)->checkState() == Qt::Checked)
                {
                    model->item(itemRow, 2)->setCheckState(Qt::Unchecked);
                }
            else
                {
                    model->item(itemRow, 2)->setCheckState(Qt::Checked);
                }
        }
}
