/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QFileInfoList>
#include <QListView>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QMessageBox>
#include <QString>
#include <QDebug>

#include "pkfindfile.h"
#include "pkconstants.h"

#include <stdio.h>

#define FILELIST "FILE LIST:\n"

PkFindFile::PkFindFile(QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle(tr("Find File"));
    setWindowIcon(QIcon(":/srcfile"));
    createForm();

    setModal(true);
}

PkFindFile::~PkFindFile()
{}


void PkFindFile::createForm()
{
    vTop = new QVBoxLayout(this);

    frame = new QGroupBox(tr("Find File inside Packages"), this);
    vTop->addWidget(frame, 4);
    vFrame = new QVBoxLayout(frame);
    value = new QLineEdit(frame);
    vFrame->addWidget(value, 1);
    value->setFocus();

    connect(value, SIGNAL(returnPressed()), this, SLOT(ok_slot()));

    tab = new QTreeWidget(frame);
    tab->setColumnCount(2);
    QStringList labels;
    labels << tr("Package") <<tr("File Name");
    tab->setHeaderLabels(labels);
    tab->setAllColumnsShowFocus(true);
    tab->sortItems(1, Qt::DescendingOrder);
    tab->setAlternatingRowColors(true);
    tab->setRootIsDecorated(false);
    vFrame->addWidget(tab, 4);

    hBox = new QHBoxLayout();
    vTop->addLayout(hBox, 0);

    ok = new QPushButton(tr("Find"), this);
    hBox->addWidget(ok, 1, Qt::AlignLeft);

    connect(ok, SIGNAL(clicked()), this, SLOT(ok_slot()));

    hBox->addStretch();

    cancel = new QPushButton(tr("Done"), this);
    hBox->addWidget(cancel, 1, Qt::AlignRight);

    connect(cancel, SIGNAL(clicked()), this, SLOT(done_slot()));

    setFixedSize(QSize(640, 480));

    setAcceptDrops(true);

    connect(tab,SIGNAL(itemClicked(QTreeWidgetItem *,int)), this, SLOT(getItemSearch(QTreeWidgetItem *,int)));

}

void PkFindFile::ok_slot()
{
    fileToSearch.clear();
    fileToSearch = value->text();
    if (fileToSearch.length() >= 3)
        {
            doFind(fileToSearch);
        }
    else
        {
            value->clear();
            QMessageBox::information(this, tr("Info "), tr("Enter more than three chars.."));
            value->setFocus();
        }
}

void PkFindFile::doFind(const QString &name)
{
    QString s, tmp;
    int cnt = 0;

    tab->clear();

    s = search(name);
    if (s.length() > 0)
        {
            int p = 0, pp = 0;
            while ((p = s.indexOf('\n', p)) >= 0)
                {
                    filesItems = new QTreeWidgetItem();

                    tmp = s.mid(pp, p - pp);
                    int t1 = tmp.indexOf('\t');
                    QString s1 = tmp.left(t1);
                    QString s2 = ((tmp.right(tmp.length() - t1)).remove(0, 2));

                    filesItems->setIcon(0, QIcon(":/slackpkg"));
                    filesItems->setText(0, s1);
                    filesItems->setText(1, s2);

                    tab->addTopLevelItem(filesItems);

                    p++;
                    pp = p;
                    cnt++;
                }
        }
    else
        {
            QTreeWidgetItem *filesItems = new QTreeWidgetItem();
            filesItems->setText(0, (tr("--Nothing found--")));
            tab->addTopLevelItem(filesItems);
        }
    tab->resizeColumnToContents(0);
    tab->resizeColumnToContents(1);
    pkBar->setPbText(tr("Find %1 items contains : %2 " ).arg(cnt).arg(name));
}

QString PkFindFile::search(const QString &str)
{
    FILE *file;
    char linebuf[1024];
    QString buf, st;
    QString fn, dr = PkConstants::instListPKG();

    QDir d(dr);
    d.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);

    if (d.exists())
        {
            pkBar->setPbText(tr("Searching File in Packages.."));
            pkBar->setProgValue(0);
            int cnt = 0 ;

            QFileInfoList list = d.entryInfoList();
            int count = list.count();
            for (QFileInfoList::Iterator it = list.begin(); it != list.end(); it++)  //   create list iterator for each file...
                {
                    QFileInfo &fi  = *it;           // pointer for traversing

                    if (!fi.isDir() && fi.isReadable())
                        {
                            fn = dr + fi.fileName();
                            file = fopen(QFile::encodeName(fn), "r");
                            if (file)
                                {
                                    while (fgets(linebuf, sizeof(linebuf), file))
                                        {
                                            if (!strcmp(linebuf, FILELIST))
                                                {
                                                    break;
                                                }
                                        }
                                    while (fgets(linebuf, sizeof(linebuf), file))
                                        {
                                            if (QString::fromLocal8Bit(linebuf).indexOf(str, 0) != -1)
                                                {
                                                    st = "/";
                                                    st += linebuf;
                                                    st.truncate(st.length() - 1);
                                                    if (st.left(8) != "/install")
                                                        {
                                                            buf += fi.fileName(); //pkg name
                                                            buf += "\t";
                                                            buf += st;            //file name
                                                            buf += "\n";
                                                        }
                                                }
                                        }
                                    fclose(file);
                                }
                        }
                    cnt ++;
                    int n = (cnt * 100) / count;
                    if (!(n % 5))
                        pkBar->setProgValue(n);

                }           // goto next list element

        }
    return buf;
}

void PkFindFile::getItemSearch(QTreeWidgetItem *item, int col)
{
    Q_UNUSED(col) //for unused variable

    QString fres = item->text(0);

    emit pkgContainFile(fres);
}

void PkFindFile::done_slot()
{
    //foreach(auto i, filesItems->takeChildren()) delete i;
    close();
}


