/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKUTILITY_H
#define PKUTILITY_H

#include <QNetworkAccessManager>
#include <QNetworkConfigurationManager>
#include <QDebug>

#include "pksetup.h"


class PkUtility:public QObject
{
    Q_OBJECT

public:
    explicit PkUtility(QObject *parent = nullptr);

    ~PkUtility();

    QString osVersion();
    QString sysInfo();
    QString sysArch();
    QString sysArchCurrent();
    bool isExec(QString &isExeFile);
    void cmdTerm(QString dirofCmd, QString fileofCmd);
    void netReplay();

public slots:

private:
    bool iSts;
    QNetworkAccessManager manager;

signals:
    void isOnLine(bool sts);
};

#include <QNetworkReply>
#include <QEventLoop>
#include <QTimer>

class Timeout : public QObject
{
    Q_OBJECT
public:
    Timeout(QNetworkReply* reply, const int timeout) : QObject(reply)
    {
        Q_ASSERT(reply);
        if (reply)
            {
                QTimer::singleShot(timeout, this, SLOT(timeout()));
            }
    }
    ~Timeout() {}

private slots:
    void timeout()
    {
        QNetworkReply* reply = static_cast<QNetworkReply*>(parent());
        if (reply->isRunning())
            {
                qDebug() << "Timedout!";
                reply->close();
            }
    }
};

#endif // PKUTILITY_H
