/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pkupgtreeview.h"


#include <QHeaderView>
#include <QString>
#include <QList>
#include <QDir>
#include <QXmlStreamReader>
#include <QCollator>
#include <QStringList>
#include <QFuture>
#include <QtConcurrent>

#include <QDebug>


PkUpgTreeView ::PkUpgTreeView (QWidget *parent) :
    QTreeView(parent)
{
    model = new QStandardItemModel();
    upgProxyModel  = new UpgSearchTreeProxyModel;

    createTree();
    headerActions();
}

PkUpgTreeView ::~PkUpgTreeView ()
{}

///
/// \brief PkUpgTreeView::loadUpgList
/// \param repository
///
void PkUpgTreeView::loadUpgList(const QString &repository)
{
    filterData(QString());
    model->removeRows(0, model->rowCount());
    populateTree(repository);

    setColumnHidden(3, true);
    unCheckAll();
    resizeCol();
    clearFocus();
}

///
/// \brief PkUpgTreeView::filterData
/// \param filter
///
void PkUpgTreeView::filterData(const QString &filter)
{
    upgProxyModel->setDynamicSortFilter(true);
    upgProxyModel->setSourceModel(model);
    upgProxyModel->setFilterKeyColumn(0);

    setModel(upgProxyModel);
    expandAll();

    upgProxyModel->setFilterWildcard(filter);
}

///
/// \brief PkUpgTreeView::getItemsChecked
/// \return
///
QStringList PkUpgTreeView::getItemsChecked()
{
    QStringList pkgChecked;

    for (int i = 0; i < model->rowCount(); ++i)
        {
            if (model->item(i, 2)->checkState() == Qt::Checked)
                {
                    pkgChecked << pkSetup.getRepo() + "/" + pkSetup.getOsVer().toLower()
                               + model->item(i, 3)->text() + "/" + model->item(i, 0)->text();
                }
        }

    return pkgChecked;
}

void PkUpgTreeView ::createTree()
{
    UpgHeader *upgHeader = new UpgHeader(Qt::Horizontal);

    setHeader(upgHeader);
    model->setHorizontalHeaderItem(0, new QStandardItem(tr("Package")));
    model->setHorizontalHeaderItem(1, new QStandardItem(tr("Status")));
    model->setHorizontalHeaderItem(2, new QStandardItem(tr("Mark")));
    model->setHorizontalHeaderItem(3, new QStandardItem("Locate"));


    setAllColumnsShowFocus(true);
    setAlternatingRowColors(true);
    setRootIsDecorated(false);

    setStyleSheet(
        "PkUpgTreeView::indicator:unchecked {image: url(:/unsel);}"
        "PkUpgTreeView::indicator:checked {image: url(:/check);}");

    connect(upgHeader, SIGNAL(customSignal(int)), SLOT(HeaderMenu(int)));
}


void PkUpgTreeView::populateTree(QString whichRepo)
{
    count = 0;
    pkBar->setProgValue(0);

    QStringList dirList = pkgUpgList(whichRepo) ;
    int countPkg = dirList.count();

    pkBar->setPbText(tr("Load Packages List..from ") + whichRepo + " Rep.");

    for (QStringList::Iterator it = dirList.begin(); it != dirList.end(); it++)
        {
            QString &namePkg = *it;
            QString pkName  = namePkg.split("|").takeFirst();

            QFile dnlFile(pkSetup.getTmpDir()+pkName);
            QIcon fIcon;

            if(dnlFile.exists())
                {
                    fIcon.addFile(":/cgreen");
                }
            else
                {
                    fIcon.addFile(":/emp");
                }

            pkg_Item = new QStandardItem(fIcon,pkName);             //name
            pkg_Item->setEditable(false);
            pkg_Item->setDragEnabled(false);
            pkg_Item->setDropEnabled(false);
            //pkgItem->setCheckable(true);
            model->setItem(count, 0, pkg_Item);


            QFuture<int> sts = QtConcurrent::run(this,&PkUpgTreeView::statusPkg,pkName);
            sts.waitForFinished();
            sts.cancel();

            QString pkgStatus;
            QIcon icon;
            if(sts == -1)
                {
                    pkgStatus = tr("Old Pkg");
                    icon.addFile(":/oldpkg");
                }
            else if(sts == 0)
                {
                    pkgStatus = tr("Same Pkg");
                    icon.addFile(":/samepkg");
                }
            else if(sts == 1)
                {
                    pkgStatus = tr("New Pkg");
                    icon.addFile(":/newpkg");
                }
            else if(sts == 2)
                {
                    pkgStatus = tr("Not Inst.");
                    icon.addFile(":/nopkg");
                }

            st_Item = new QStandardItem(icon,pkgStatus);                       //Status
            st_Item->setEditable(false);
            st_Item->setDragEnabled(false);
            st_Item->setDropEnabled(false);
            model->setItem(count, 1, st_Item);

            ck_Item = new QStandardItem();                                       //check
            ck_Item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
            ck_Item->setData(Qt::Unchecked, Qt::CheckStateRole);
            ck_Item->setCheckable(true);
            ck_Item->setSelectable(true);
            ck_Item->setEditable(false);
            ck_Item->setDragEnabled(false);
            ck_Item->setDropEnabled(false);
            ck_Item->setSizeHint(QSize(20, 22));
            model->setItem(count, 2, ck_Item);

            loc_Item = new QStandardItem(namePkg.split("|").takeLast());         //locate
            loc_Item->setEditable(false);
            loc_Item->setDragEnabled(false);
            loc_Item->setDropEnabled(false);
            model->setItem(count, 3, loc_Item);

            count++;

            int n = (count * 100) / countPkg;
            if (!(n % 5))
                {
                    pkBar->setProgValue(n);
                }
        }
}


QStringList PkUpgTreeView::pkgUpgList(QString upgPackages)
{
    QString element;

    QFile xmlFile(getFileRepo(upgPackages));

    if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox::critical(this,tr("Load XML File Problem"),
                                  tr("Couldn't open %1 to load repository packages").arg(xmlFile.fileName()),
                                  QMessageBox::Ok);
            // return;
        }
    xmlReader = new QXmlStreamReader();
    xmlReader->setDevice(&xmlFile);
    QStringList xmlListPackages;

    //Parse the XML until we reach end of it
    while(!xmlReader->atEnd() && !xmlReader->hasError())
        {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            //If token is just StartDocument - go to next
            if(token == QXmlStreamReader::StartDocument)
                {
                    continue;
                }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement)
                {
                    if(xmlReader->name() == "name")
                        {
                            //continue;
                            //xmlListPackages << xmlReader->readElementText();
                            element = xmlReader->readElementText();
                        }
                    if(xmlReader->name() == "location")
                        {
                            //xmlListPackages << xmlReader->readElementText();
                            element += "|" + xmlReader->readElementText();
                            xmlListPackages << element;
                        }
                }
        }

    if(xmlReader->hasError())
        {
            QMessageBox::critical(this,
                                  "xmlFile.xml Parse Error",xmlReader->errorString(),
                                  QMessageBox::Ok);
            //return;
        }

    //close reader and flush file
    xmlReader->clear();
    xmlFile.close();
    return xmlListPackages;
}

///
/// \brief PkUpgTreeView::getFileRepo get xml file on selected repository
/// \return
///
QString PkUpgTreeView::getFileRepo(QString repos)
{
    QString file;

    if(repos.contains("current"))
        {
            file = pkSetup.getDefDir()+PkConstants::localCurrentFile().split(".").takeFirst()+".xml";
        }
    else
        {
            file = pkSetup.getDefDir()+PkConstants::localPatchFile().split(".").takeFirst()+".xml";
        }
    return file;
}

///
/// \brief PkUpgTreeView::statusPkg
/// \param repPkgName
/// \return 2 if package is not present or installed
///
int PkUpgTreeView::statusPkg(QString repPkgName)
{
    QString nameFromRep = repPkgName.section('-',0,repPkgName.count("-") - 3);

    QStringList dirList = localPkgList();
    int res = 2;

    for (QStringList::Iterator it = dirList.begin(); it != dirList.end(); it++)
        {
            QString &namePkg = *it;

            QString nameFromLoc = namePkg.section('-',0,namePkg.count("-") - 3);

            if (nameFromRep == nameFromLoc)
                {
                    QFuture<int> cmp = QtConcurrent::run(this,&PkUpgTreeView::pkgCompare,repPkgName.section('-',1,repPkgName.count("-") -2),
                                                         namePkg.section('-',1,namePkg.count("-") - 2));
                    cmp.waitForFinished();
                    cmp.cancel();
                    res = cmp;

                    break;
                }
        }
    return res;
}

///
/// \brief PkUpgTreeView::pkgCompare Compare alpha and numeric segments of two versions.
/// return 1: a is newer than b
///        0: a and b are the same version
///       -1: b is newer than a
/// \param pkbA from repository
/// \param pkgB from local
/// \return
///
int PkUpgTreeView::pkgCompare(QString pkgA, QString pkgB)
{
    /* QCollator coll;

     int res = coll.compare(pkgA,pkgB);
     return res;*/

    // vnum stores each numeric
    // part of version
    int vnum1 = 0, vnum2 = 0;

    // loop until both string are
    // processed
    for (int i = 0, j = 0; (i < pkgA.length() || j < pkgB.length());)
        {
            // storing numeric part of
            // version 1 in vnum1
            while (i < pkgA.length() && pkgA[i] != '.')
                {
                    vnum1 = vnum1 * 10 + (pkgA[i].toLatin1() - '0');
                    i++;
                }

            // storing numeric part of
            // version 2 in vnum2
            while (j < pkgB.length() && pkgB[j] != '.')
                {
                    vnum2 = vnum2 * 10 + (pkgB[j].toLatin1() - '0');
                    j++;
                }

            if (vnum1 > vnum2)
                return 1;
            if (vnum2 > vnum1)
                return -1;

            // if equal, reset variables and
            // go for next numeric part
            vnum1 = vnum2 = 0;
            i++;
            j++;
        }
    return 0;
}

///
/// \brief PkUpgTreeView::resizeCol
///
void PkUpgTreeView::resizeCol()
{
    int nColums = model->columnCount();

    for (int col = 0 ; col < nColums ; col ++)
        {
            resizeColumnToContents(col);
        }
}

///
/// \brief PkUpgTreeView::localPkgList retun local packages list
/// \return
///
QStringList PkUpgTreeView::localPkgList()
{
    PkSetup pkSetup;
    QString element;
    QStringList xmlListPackages;

    QFile xmlFile(pkSetup.getFileListPkg());

    if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox::critical(this,tr("Load XML File Problem"),
                                  tr("Couldn't open %1 to load local packages").arg(xmlFile.fileName()),
                                  QMessageBox::Ok);
            xmlListPackages.clear();
            return xmlListPackages;
        }

    xmlReader = new QXmlStreamReader();
    xmlReader->setDevice(&xmlFile);

    //Parse the XML until we reach end of it
    while(!xmlReader->atEnd() && !xmlReader->hasError())
        {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            //If token is just StartDocument - go to next
            if(token == QXmlStreamReader::StartDocument)
                {
                    continue;
                }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement)
                {
                    if(xmlReader->name() == "name")
                        {
                            element = xmlReader->readElementText();
                            xmlListPackages << element;
                        }
                }
        }

    if(xmlReader->hasError())
        {
            QMessageBox::critical(this,
                                  "xmlFile.xml Parse Error",xmlReader->errorString(),
                                  QMessageBox::Ok);
            //return;
        }

    //close reader and flush file
    xmlReader->clear();
    xmlFile.close();
    return xmlListPackages;
}

///
/// \brief PkUpgTreeView::headerActions
///
void PkUpgTreeView::headerActions()
{
    resizeUpgTree = new QAction(QIcon(":/unsel"), tr("Resize "),  this);
    resizeUpgTree->setToolTip(tr("Resize by Content"));
    menu0.addAction(resizeUpgTree);

    //--------------------------------

    sortingNew = new QAction(QIcon(":/unsel"), tr("New "),  this);
    sortingOld = new QAction(QIcon(":/unsel"), tr("Old "),  this);
    menu1.addAction(sortingNew);
    menu1.addAction(sortingOld);

    //--------------------------------

    upgUncheck = new QAction(QIcon(":/unsel"), tr("Uncheck All"), this);
    upgUncheck->setObjectName(tr("uncheck"));
    menu2.addAction(upgUncheck);
}

///
/// \brief PkUpgTreeView::HeaderMenu
/// \param section
///
void PkUpgTreeView::HeaderMenu(int section)
{
    switch (section)
        {
        case 0:
        {
            connect(resizeUpgTree, SIGNAL(hovered()), this, SLOT(resizeCol()));
            menu0.popup(QCursor::pos());
        }
        break;
        case 1 :
        {
            connect(sortingNew, SIGNAL(triggered()), this, SLOT(sortDataNew()));
            connect(sortingOld, SIGNAL(triggered()), this, SLOT(sortDataOld()));
            menu1.popup(QCursor::pos());
        }
        break;
        case 2 :
        {
            connect(upgUncheck, SIGNAL(triggered()), this, SLOT(unCheckAll()));
            menu2.popup(QCursor::pos());
        }
        break;
        default:
            break;
        }
}

///
/// \brief PkUpgTreeView::sortDataNew
///
void PkUpgTreeView::sortDataNew()
{
    model->sort(1,Qt::AscendingOrder);
}

///
/// \brief PkUpgTreeView::sortDataOld
///
void PkUpgTreeView::sortDataOld()
{
    model->sort(1,Qt::DescendingOrder);
}

///
/// \brief PkUpgTreeView::unCheckAll
///
void PkUpgTreeView::unCheckAll()
{
    for (int i = 0; i < model->rowCount(); ++i)
        {
            if (model->item(i, 2)->checkState() == Qt::Checked)
                {
                    model->item(i, 2)->setCheckState(Qt::Unchecked);
                }
        }
}

///
/// \brief PkUpgTreeView::keyPressEvent
/// \param event
///
void PkUpgTreeView::keyPressEvent(QKeyEvent* event) //Not work if row is selected by mark coloum !
{
    QTreeView::keyPressEvent(event);

    QModelIndex indexFilter = upgProxyModel->mapToSource(currentIndex());

    if(indexFilter.isValid() && event->key() == Qt::Key_Space)
        {
            int itemRow  = indexFilter.row();

            if (model->item(itemRow, 2)->checkState() == Qt::Checked)
                {
                    model->item(itemRow, 2)->setCheckState(Qt::Unchecked);
                }
            else
                {
                    model->item(itemRow, 2)->setCheckState(Qt::Checked);
                }
        }
}


