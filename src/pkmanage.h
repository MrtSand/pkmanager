/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKMANAGE_H
#define PKMANAGE_H

#include <QDialog>
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QStandardItem>
#include <QFileInfoList>
#include <QCheckBox>
#include <QTextEdit>
#include <QProcess>

#define instPKG   "Install Packages"
#define uninstPKG "Uninstall Packages"
#define remPKG    "Delete Packages"

class PkManage : public QDialog
{
    Q_OBJECT
public:
    explicit PkManage(QWidget *parent = 0);
    ~PkManage();

    void managePkg(const QString &actType, QFileInfoList pkgList);

private:
    QFileInfoList pkFileList;
    QString ActionType;
    QLabel *title, *strDebug;
    QVBoxLayout *leftLayout, *rightLayout, *mainLayout;
    QHBoxLayout *butLayout, *hLayout, *dLayout;
    QTreeWidget *pkList;
    QTreeWidgetItem *pkListItem;
    QPushButton *actionButton, *closeButton,*stopProcButton;
    QCheckBox *upgradeSel, *testInst, *notClose, *verDel,*isDebug;
    QTextEdit *term;
    QProcess *pkTerm;
    QString textDebug(QString text);

    void createForm();
    void clearSets();
    void setWidget();
    void loadPkList(QFileInfoList fileList);
    QString cmdString();

    void execCmdStr(QString cmd);

signals:
    void manageFinished();

public slots:

private slots:
    void isStarted();
    void showOutput();
    void actionComplete(int res);
    void selUpgrade();
    void selTestInst();
    void selVerDel();
    void selDebug();
    void pkActionButtClicked();
    void stopProcess();
    void cancelButtClicked();

};

class MyStringList : public QStringList
{
public:
    operator QString() const
    {
        return this->join(" ");
    }
};

#endif // PKMANAGE_H
