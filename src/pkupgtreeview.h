/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKUPGTREEVIEW_H
#define PKUPGTREEVIEW_H

#include "pkstatusbar.h"
#include "pkconstants.h"
#include "pksetup.h"

#include <QTreeView>
#include <QStandardItemModel>
#include <QFileInfoList>
#include <QXmlStreamReader>
#include <QMenu>
#include <QAction>
#include <QActionGroup>

class PkStatusBar;
class UpgSearchTreeProxyModel;

class PkUpgTreeView : public QTreeView
{
    Q_OBJECT
public:
    explicit  PkUpgTreeView (QWidget *parent = 0);
    ~PkUpgTreeView ();

    PkStatusBar *pkBar;
    PkSetup pkSetup;

    void filterData(const QString &filter);
    void loadUpgList(const QString &repository);
    QStringList getItemsChecked();

private :
    QStandardItemModel *model;
    UpgSearchTreeProxyModel *upgProxyModel;

    int count;
    QXmlStreamReader *xmlReader;

    QStandardItem *pkg_Item;
    QStandardItem *st_Item;
    QStandardItem *ck_Item;
    QStandardItem *loc_Item;


    QString getFileRepo(QString repos);
    QStringList pkgUpgList(QString upgPackages);
    QStringList localPkgList();
    int pkgCompare(QString pkgA, QString pkgB);
    int statusPkg(QString repPkgName);

    QPoint pos;
    QMenu menu0, menu1, menu2;

    QAction *resizeUpgTree;
    QAction *sortingNew;
    QAction *sortingOld;
    QAction *upgUncheck;

    void createTree();
    void populateTree(QString whichRepo);

    void headerActions();

public slots:
    void unCheckAll();

private slots:
    void HeaderMenu(int section);
    void resizeCol();
    void sortDataNew();
    void sortDataOld();

protected:
    virtual void keyPressEvent(QKeyEvent *event);


};

#include <QHeaderView>
#include <QMouseEvent>
class UpgHeader :public QHeaderView
{
    Q_OBJECT
public:
    using QHeaderView::QHeaderView;
protected:
    void mousePressEvent(QMouseEvent *event)
    {
        QHeaderView::mousePressEvent(event);
        if(event->buttons() == Qt::RightButton)
            emit customSignal(logicalIndexAt(event->pos()));
    }
signals:
    void customSignal(int section);
};


#include <QSortFilterProxyModel>
class UpgSearchTreeProxyModel : public QSortFilterProxyModel
{
public:
    UpgSearchTreeProxyModel(QObject *parent = 0)
        : QSortFilterProxyModel(parent)
    {
        setFilterCaseSensitivity(Qt::CaseInsensitive);
    }

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
    {
        QModelIndex idx = sourceModel()->index(source_row, 0, source_parent);
        if (sourceModel()->hasChildren(idx))
            return (true);
        return (QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent));

    }
};

#endif // PKUPGTREEVIEW_H
