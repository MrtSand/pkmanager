/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKSTATUSBAR_H
#define PKSTATUSBAR_H

#include <QStatusBar>
#include <QLabel>
#include <QProgressBar>
#include <QQueue>

class PkStatusBar : public QStatusBar
{
    Q_OBJECT
public:
    explicit PkStatusBar(QWidget *parent = 0);
    ~PkStatusBar();

    void setPbText(QString datastr);
    void setProgValue(int value);
    void stsConn(bool statusConn);

private:
    QLabel *lbIcon;
    QProgressBar *statusPBar;
    QMovie *movie;
    QQueue<QString> queue;

public slots:

private slots:

    void hideWidgs();

signals:

};

#endif // PKSTATUSBAR_H
