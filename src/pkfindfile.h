/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKFINDFILE_H
#define PKFINDFILE_H

#include <QDialog>
#include <QPushButton>
#include <QGroupBox>
#include <QLineEdit>
#include <QTreeWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidgetItem>

#include  "pkstatusbar.h"


class PkFindFile : public QDialog
{
    Q_OBJECT
public:
    explicit PkFindFile(QWidget *parent = 0);
    ~PkFindFile();
    PkStatusBar *pkBar;

private:

    QPushButton *ok, *cancel;
    QGroupBox *frame;
    QLineEdit *value;
    QString fileToSearch;
    QTreeWidget *tab;
    QVBoxLayout *vTop, *vFrame;
    QHBoxLayout *hBox;
    QTreeWidgetItem *filesItems;

    void createForm();
    void doFind(const QString &name);
    QString search(const QString &str);

signals:
    void pkgContainFile(QString fres);

private slots:
    void done_slot();
    void ok_slot();
    void getItemSearch(QTreeWidgetItem *item, int col);

};

#endif // PKFINDFILE_H
