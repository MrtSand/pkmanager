/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pknetfunc.h"
#include "pksetup.h"
#include "pkconstants.h"
#include "pkutility.h"

#include <QStringList>
#include <QFuture>
#include <QtConcurrent>

PkNetFunc::PkNetFunc(QWidget *parent): QObject(parent)
{
    pkSetup = new PkSetup();

    downloadedCount = 0;
    totalCount = 0;
}

PkNetFunc::~PkNetFunc()
{}

///
/// \brief PkNetFunc::dwlMirrorUrls Read Https page and save list of mirrors
///
bool PkNetFunc::manageUrlsMirror()
{
    bool res = false ;
    QStringList mirrorList;
    mirrorList.clear();

    int days = pkSetup->getDays();

    QFile mirrorFile(pkSetup->getDefDir()+PkConstants::urlMirrFile());
    QDate tData(QDate::currentDate());

    QFileInfo infMirFile(mirrorFile);

    if(!mirrorFile.exists() || infMirFile.metadataChangeTime().date().daysTo(tData) > days)
        {

            QFuture<void> f1 = QtConcurrent::run(this,&PkNetFunc::dnlHttpMirrors);
            f1.waitForFinished();
            res = f1.isFinished();
        }
    return res;
}

///
/// \brief PkNetFunc::downMirrors  Read https mirrors url and save file
/// \return mirror list
///
void PkNetFunc::dnlHttpMirrors()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager();
    QNetworkRequest netRequest(PkConstants::mirrorSite());

    QSslConfiguration conf = netRequest.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    netRequest.setSslConfiguration(conf);

    QNetworkReply *netReply = manager->get(netRequest);
    QEventLoop loop;
    connect(netReply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(netReply->error())
        {
            qDebug() << netReply->errorString();
        }
    else
        {
            QString htmlList  =  netReply->readAll();

            if(!htmlList.isEmpty())
                {
                    QStringList inp, out;

                    inp <<  htmlList.split('\n',Qt::SkipEmptyParts);

                    QStringList::Iterator it = nullptr;

                    for (it = inp.begin(); it !=  inp.end(); ++it)
                        {
                            QString current = *it;

                            if (current.contains("Last Updated:"))
                                {
                                    int len = current.length();
                                    out += (current.mid(4,len-9));
                                }

                            if (current.contains("<a href=https://"))
                                {

                                    int str = current.indexOf("=")+1;
                                    int stp = current.indexOf("/>")-str;

                                    out .append(current.mid(0,2) + "\t" + current.mid(str,stp));
                                }
                        }
                    QFile file(pkSetup->getDefDir()+"mirrors");

                    if(file.open( QIODevice::WriteOnly ))
                        {
                            QTextStream textStream( &file);
                            textStream  <<out.join("\n");
                            file.close();
                        }
                    emit textToStBar(tr("Mirrors Urls file save ! "));
                    netReply->deleteLater();
                }
            else
                {
                    netReply->deleteLater();
                }
        }
}

///
/// \brief PkNetFunc::dwlMirrorPackageTxt  Download List packages and List patchs
///
bool PkNetFunc::manageMirrorPackageTxt()
{
    bool res = false;
    QStringList args;
    args.clear();
    QDate tData(QDate::currentDate());

    QFile releaseFile(pkSetup->getDefDir()+PkConstants::localPatchFile());
    QFileInfo infReleaseFile(releaseFile);

    if(!releaseFile.exists() || infReleaseFile.metadataChangeTime().date().daysTo(tData) > PkConstants::updateDays())
        {
            args  <<  pkSetup->getFullMirrorPatchPath();
        }

    QFile currentFile(pkSetup->getDefDir()+PkConstants::localCurrentFile());
    QFileInfo infCurrentFile(currentFile);

    if(!currentFile.exists() || infCurrentFile.metadataChangeTime().date().daysTo(tData) > PkConstants::updateDays())
        {
            args  << pkSetup->getFullMirrorPath();
        }

    if(!args.isEmpty())
        {
            QStringList::Iterator it = nullptr;

            for (it = args.begin(); it !=  args.end(); ++it)
                {
                    QString arg = *it;

                    QFuture<void> f1= QtConcurrent::run(this,&PkNetFunc::parseHttpRemotePkgLists, arg);
                    f1.waitForFinished();
                    res = f1.isFinished();
                }
        }

    return res;
}

///
/// \brief PkNetFunc::parseHttpRemotePkgLists Read and save xml of patch and current
/// \param packageList
///
void PkNetFunc::parseHttpRemotePkgLists(QString packageList)
{
    QString basename;
    if (packageList.contains("PACKAGES.TXT"))
        {
            if(packageList.contains("-current"))
                {
                    basename = PkConstants::localCurrentFile();
                }
            else
                {
                    basename=PkConstants::localPatchFile();
                }
        }

    QFile file(pkSetup->getDefDir()+basename);


    QNetworkAccessManager *manager = new QNetworkAccessManager();
    QNetworkRequest netRequest(packageList);

    QSslConfiguration conf = netRequest.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    netRequest.setSslConfiguration(conf);

    QNetworkReply *netReply = manager->get(netRequest);
    QEventLoop loop;
    connect(netReply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(netReply->error())
        {
            qDebug() << netReply->errorString();
        }
    else
        {
            QString htmlList  =  netReply->readAll();

            if(!htmlList.isEmpty())
                {
                    QStringList inp;

                    inp <<  htmlList.split('\n',Qt::SkipEmptyParts);
                    if ( file.open( QIODevice::WriteOnly ) )
                        {
                            QXmlStreamWriter xmlWriter(&file);
                            xmlWriter.setAutoFormatting(true);  // Set the auto-formatting text
                            xmlWriter.writeStartDocument();     // run record in a document
                            xmlWriter.writeStartElement("Packages");

                            QStringList::Iterator it = nullptr;
                            int i = 1;

                            for (it = inp.begin(); it !=  inp.end(); ++it)
                                {
                                    QString current = *it;

                                    if(current.contains("PACKAGES.TXT;"))
                                        {
                                            xmlWriter.writeStartElement("date");
                                            xmlWriter.writeCharacters(current.split(";").takeLast().simplified());   // Write the date
                                            xmlWriter.writeEndElement();
                                        }
                                    if(current.contains("PACKAGE NAME:"))
                                        {
                                            xmlWriter.writeStartElement(QString("pkg_%1").arg(i));                  // Write the number
                                            xmlWriter.writeStartElement("name");
                                            xmlWriter.writeCharacters(current.split(":").takeLast().simplified());  // Write the name
                                            xmlWriter.writeEndElement();
                                            i++;
                                        }
                                    if(current.contains("PACKAGE LOCATION:"))
                                        {
                                            xmlWriter.writeStartElement("location");
                                            xmlWriter.writeCharacters("/" + current.split("./").takeLast().simplified());  // Write the location
                                            xmlWriter.writeEndElement();
                                            xmlWriter.writeEndElement();
                                        }

                                }
                            xmlWriter.writeEndElement();
                            xmlWriter.writeEndDocument();
                            file.flush();
                            file.close();
                            emit textToStBar(tr("%1 file save ! ").arg(QFileInfo(file).baseName()));
                        }
                }
        }
}


///
/// \brief PkNetFunc::doDownload download multiple packages or file from mirror selected
/// \param urls
///
void PkNetFunc::doDownload(const QStringList &urls)
{
    downloadedCount = 0;
    totalCount = 0;

    for (const QString &urlAsString : urls)
        append(QUrl::fromEncoded(urlAsString.toLocal8Bit()));

    if (downloadQueue.isEmpty())
        QTimer::singleShot(0, this, SLOT(finished()));
}

void PkNetFunc::append(const QUrl &url)
{
    if (downloadQueue.isEmpty())
        {
            QTimer::singleShot(0, this, SLOT(startNextDownload()));
        }
    downloadQueue.enqueue(url);
    ++totalCount;
}

///
/// \brief PkNetFunc::saveFileName save file name
/// \param url
/// \return
///
QString PkNetFunc::saveFileName(const QUrl &url)
{
    QString path = url.path();
    QString basename = QFileInfo(path).fileName();

    if (basename.isEmpty())
        basename = "PKG_Unknow";

    return basename;
}

void PkNetFunc::startNextDownload()
{
    if (downloadQueue.isEmpty())
        {
            emit textToMngDnl(tr(" %1 of %2 files downloaded successfully \n").arg(downloadedCount).arg(totalCount));

            QList<QByteArray> list = currentDownload->rawHeaderList();
            foreach (QByteArray header, list)
                {
                    QString qsLine = QString(header) + " = " + currentDownload->rawHeader(header);
                    emit textToMngDnl(qsLine);
                }

            emit dnwlFinished();

            return;
        }

    QUrl url = downloadQueue.dequeue();
    QString filename = pkSetup->getTmpDir()+saveFileName(url);

    output.setFileName(filename);
    if (!output.open(QIODevice::WriteOnly))
        {
            emit textToMngDnl(tr("Problem save file %1 for download %2: %3")
                              .arg(filename,url.toEncoded().constData(),output.errorString()));
            startNextDownload();
            return;                 // skip this download
        }

    QNetworkRequest request(url);

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    currentDownload = manager.get(request);

    connect(currentDownload, &QNetworkReply::downloadProgress, this, &PkNetFunc::downloadProgress);
    connect(currentDownload, &QNetworkReply::finished, this, &PkNetFunc::downloadFinished);
    connect(currentDownload, &QNetworkReply::readyRead, this, &PkNetFunc::downloadReadyRead);

    // prepare the output
    emit textToMngDnl(tr("Downloading %1").arg(url.toEncoded().constData()));
    downloadTimer.start();
}


void PkNetFunc::downloadProgress(qint64 received, qint64 total)
{
    double downloadSpeedInfo = received * 1000.0 / downloadTimer.elapsed();
    double downloadSpeed  = downloadSpeedInfo;
    int progress = (100*received)/total;
    emit valueToMngDnl(progress);

    size = sizeHuman(total);

    QString unit;
    if(downloadSpeedInfo < 1024) unit = "bytes/sec";
    else if(downloadSpeedInfo < 1024 * 1024)
        {
            downloadSpeedInfo /= 1024;
            unit = "kB/s";
        }
    else
        {
            downloadSpeedInfo /= (1024 * 1024);
            unit = "MB/s";
        }

    QString speed = QString::fromLatin1("%1%2").arg(downloadSpeedInfo,3,'f',1).arg(unit);

    int secondsToFinishDownload = (total-received)/downloadSpeed;
    QString timeLeft = timeHuman(secondsToFinishDownload);

    QString res = QString::fromLatin1("%1 - %2 - %3").arg(size).arg(speed).arg(timeLeft);

    emit valueSpeed(res);
}

QString PkNetFunc::sizeHuman(qint64 fileSize)
{
    QStringList sizeExt;
    sizeExt << "KB" << "MB" << "GB" << "TB";

    QStringListIterator it(sizeExt);
    QString units("bytes");

    float num = fileSize;

    while(num >= 1024.0 && it.hasNext())
        {
            units = it.next();
            num /= 1024.0;
        }

    return QString().setNum(num, 'f', 2) + units;
}

QString PkNetFunc::timeHuman(const int secondsToFinishDownload)
{
    int seconds = secondsToFinishDownload%60;
    int minutes = secondsToFinishDownload/60;
    int hours = minutes/60;
    minutes%=60;

    QString secondsFormat = seconds < 10 ? "0" + QString::number(seconds) : QString::number(seconds);
    QString minutesFormat = minutes < 10 ? "0" + QString::number(minutes) : QString::number(minutes);
    QString hoursFormat = hours < 10 ? "0" + QString::number(hours) : QString::number(hours);

    return hoursFormat + ":" + minutesFormat + ":" + secondsFormat;
}

void PkNetFunc::abort()
{
    disconnect(currentDownload, &QNetworkReply::finished, this, &PkNetFunc::downloadFinished);
    disconnect(currentDownload, &QNetworkReply::downloadProgress, this, &PkNetFunc::downloadProgress);

    currentDownload->abort();
    currentDownload = nullptr;

    output.close();
    output.remove();

    emit textToMngDnl(tr("Stop Download by User. Delete %1 \n").arg(output.fileName()));
}


void PkNetFunc::downloadFinished()
{
    output.close();

    if (currentDownload->error())
        {
            // download failed
            emit  textToMngDnl(tr("Failed: %1").arg(currentDownload->errorString()));

            output.remove();
        }
    else
        {
            // let's check if it was actually a redirect
            if (isHttpRedirect())
                {
                    reportRedirect();
                    output.remove();
                }
            else
                {
                    textToMngDnl(tr("Succeeded."));
                    ++downloadedCount;
                }
        }

    currentDownload->deleteLater();
    startNextDownload();
}

void PkNetFunc::downloadReadyRead()
{
    output.write(currentDownload->readAll());
}

bool PkNetFunc::isHttpRedirect() const
{
    int statusCode = currentDownload->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return statusCode == 301 || statusCode == 302 || statusCode == 303
           || statusCode == 305 || statusCode == 307 || statusCode == 308;
}

void PkNetFunc::reportRedirect()
{
    int statusCode = currentDownload->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QUrl requestUrl = currentDownload->request().url();

    QVariant target = currentDownload->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (!target.isValid())
        return;
    QUrl redirectUrl = target.toUrl();
    if (redirectUrl.isRelative())
        redirectUrl = requestUrl.resolved(redirectUrl);

    emit  textToMngDnl(tr("Request: %1 was redirected with code: %2\nRedirected to: %3")
                       .arg(requestUrl.toString()).arg(statusCode).arg(redirectUrl.toString()));
}
