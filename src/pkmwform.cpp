/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include "pkmainwindow.h"

void PkMainWindow::createForm()
{
    QWidget *main = new QWidget;

    // splitter
    top = new QBoxLayout(QBoxLayout::TopToBottom);
    vPan = new QSplitter(Qt::Horizontal);
    top->addWidget(vPan);

    // the left panel
    leftpanel = new QFrame(vPan);
    leftbox = new QBoxLayout(QBoxLayout::TopToBottom, leftpanel);

    // the right panel
    rightpanel = new QFrame(vPan);
    rightbox = new QBoxLayout(QBoxLayout::TopToBottom, rightpanel);

    // both  panel
    mainLayout = new QGridLayout(main);
    mainLayout->setSpacing(1);
    mainLayout->addWidget(vPan);

    // statusBar
    pkStatusBar = new PkStatusBar(this);
    setStatusBar(pkStatusBar);

    // On the left
    searchBox = new QBoxLayout(QBoxLayout::LeftToRight);
    searchLabel = new QLabel(tr("Search Package: "));
    searchBox->addWidget(searchLabel);
    pkSLine = new PkSearchLine();
    searchBox->addWidget(pkSLine);

    numpkg = new QLabel("");
    numpkg->setToolTip(tr("Num Package Installed"));
    searchBox->addWidget(numpkg);
    leftbox->addLayout(searchBox);

    //package treewiev inst
    ltreev = new QBoxLayout(QBoxLayout::LeftToRight);

    ltabWidget = new QTabWidget();
    instTreeView  = new PkTreeView();
    instTreeView->pkBar = pkStatusBar;
    ltabWidget->addTab(instTreeView, tr("Packages Installed"));

    //package treewiev uninst
    uninstTreeView  = new PkTreeView();
    uninstTreeView->pkBar = pkStatusBar;
    ltabWidget->addTab(uninstTreeView, tr("Packages Uninstalled"));

    //package treewiev patch
    upgTreeView  = new PkUpgTreeView();
    upgTreeView->pkBar = pkStatusBar;
    ltabWidget->addTab(upgTreeView,"Packages ..");

    ltreev->addWidget(ltabWidget);
    leftbox->addLayout(ltreev);

    // On the rigth
    rtreev = new QBoxLayout(QBoxLayout::LeftToRight);
    rtabWidget = new QTabWidget();
    infoText = new PkInfo();
    rtabWidget->addTab(infoText, tr("System Information"));

    pkInfo = new PkInfo();
    pkInfo->pkBar = pkStatusBar;
    rtabWidget->addTab(pkInfo, tr("Package Info"));

    pkFiles = new PkFiles();
    pkFiles->pkBar=pkStatusBar;
    rtabWidget->addTab(pkFiles, tr("Package Files"));

    rtreev->addWidget(rtabWidget);
    rightbox->addLayout(rtreev);

    QList<int> vPanSize;
    vPanSize.push_back(2500);
    vPanSize.push_back(1800);
    vPan->setSizes(vPanSize);

    setCentralWidget(main);

    instTreeView->filterData("");

    connect(pkSLine,SIGNAL(textChanged(QString)), this, SLOT(filterTreeView()));
    //connect(pkSLine,SIGNAL(searchLineClean()), this, SLOT(updateInfos())); // NOTE togliere
    connect(ltabWidget,SIGNAL(currentChanged(int)), this, SLOT(tabLSelected(int)));

    connect(instTreeView, SIGNAL(itemSel(QString)),this,SLOT(ShowPkgInfo(QString)));

}

void PkMainWindow::createActions()
{
    openAction = new QAction(tr("&Open"), this);
    openAction->setIcon(QIcon(":/inspkg"));
    openAction->setShortcut(tr("Alt+o"));
    openAction->setToolTip(tr("Open package file"));
    connect(openAction, SIGNAL(triggered()), this, SLOT(installPkg()));

    remPkgAction = new QAction(tr("&Remove"), this);
    remPkgAction->setIcon(QIcon(":/rempkg"));
    remPkgAction->setShortcut(tr("Alt+R"));
    remPkgAction->setToolTip(tr("Remove package file"));
    connect(remPkgAction, SIGNAL(triggered()), this, SLOT(removePkg()));

    reloadAction = new QAction(tr("&Reload"), this);
    reloadAction->setIcon(QIcon(":/reload"));
    reloadAction->setShortcut(tr("Alt+r"));
    reloadAction->setToolTip(tr("Reload Package"));
    connect(reloadAction, SIGNAL(triggered()), this, SLOT(loadLocalPkgLists()));

    delPkgAction = new QAction(tr("&Delete"), this);
    delPkgAction->setIcon(QIcon(":/delpkg"));
    delPkgAction->setShortcut(tr("Alt+d"));
    delPkgAction->setToolTip(tr("Delete Package"));
    delPkgAction->setEnabled(false);
    connect(delPkgAction, SIGNAL(triggered()), this, SLOT(deletePkg()));

    searchPkgAction = new QAction(tr("&Search Package "), this);
    searchPkgAction->setIcon(QIcon(":/srcpkg"));
    searchPkgAction->setShortcut(tr("Alt+p"));
    searchPkgAction->setToolTip(tr("Search Package"));
    connect(searchPkgAction, SIGNAL(triggered()), this, SLOT(searchPkg()));

    searchFileAction = new QAction(tr("&Search File "), this);
    searchFileAction->setIcon(QIcon(":/srcfile"));
    searchFileAction->setShortcut(tr("Alt+f"));
    searchFileAction->setToolTip(tr("Search File"));
    connect(searchFileAction, SIGNAL(triggered()), this, SLOT(searchFile()));

    openSetup = new QAction(tr("&Setup"), this);
    openSetup->setIcon(QIcon(":/setup"));
    openSetup->setToolTip(tr("Set Settings"));
    connect(openSetup, SIGNAL(triggered()), this, SLOT(pkDSetup()));

    aboutAction = new QAction(tr("&About"), this);
    aboutAction->setIcon(QIcon(":/pkmanager"));
    aboutAction->setToolTip(tr("Show the application's About box"));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));

    aboutHelp = new QAction(tr("&Help"), this);
    aboutHelp->setToolTip(tr("Show the application's Help"));
    aboutHelp->setShortcut( QKeySequence(Qt::Key_F1) );
    connect(aboutHelp, SIGNAL(triggered()), this, SLOT(showHelp()));

    aboutQtAction = new QAction(tr("About &Qt"), this);
    aboutQtAction->setToolTip(tr("Show the Qt library's About box"));
    connect(aboutQtAction, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    hideAction = new QAction(tr("&Hide"), this);
    hideAction->setIcon(QIcon(":/ghost"));
    connect(hideAction, &QAction::triggered, this, &QWidget::hide);

    exitAction = new QAction(tr("&Quit"), this);
    exitAction->setIcon(QIcon(":/exit"));
    exitAction->setShortcut(tr("Alt+q"));
    exitAction->setToolTip(tr("Quit"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(closeApp()));
}

void PkMainWindow::createMenu()
{
    fileMenu = menuBar()->addMenu(tr("&Main"));
    fileMenu->addAction(openAction);
    fileMenu->addAction(remPkgAction);
    fileMenu->addAction(reloadAction);
    fileMenu->addAction(delPkgAction);
    fileMenu->addSeparator();
    fileMenu->addAction(searchPkgAction);
    fileMenu->addAction(searchFileAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    setupMenu = menuBar()->addMenu(tr("&Settings"));
    setupMenu->addAction(openSetup);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(aboutHelp);
    helpMenu->addAction(aboutQtAction);
}

void PkMainWindow::createToolBar()
{
    toolBar = new QToolBar();
    toolBar->setObjectName(QString::fromUtf8("toolBar"));
    addToolBar(Qt::LeftToolBarArea, toolBar);
    toolBar->contextMenuPolicy();
    toolBar->setMovable(false);

    btnOpen = new QToolButton;
    btnOpen->setIcon(QIcon(":/inspkg"));
    btnOpen->setToolTip(tr("Packages Browser"));

    btnRemove = new QToolButton;
    btnRemove->setIcon(QIcon(":/rempkg"));
    btnRemove->setToolTip(tr("Package Remove"));

    btnReload = new QToolButton;
    btnReload->setIcon(QIcon(":/reload"));
    btnReload->setToolTip(tr("Reload Package List"));

    btnDel = new QToolButton;
    btnDel->setIcon(QIcon(":/delpkg"));
    btnDel->setToolTip(tr("Delete Package"));
    btnDel->setEnabled(false);

    btnSrcPkg = new QToolButton;
    btnSrcPkg->setIcon(QIcon(":/srcpkg"));
    btnSrcPkg->setToolTip(tr("Search Package"));

    btnSrcFile = new QToolButton;
    btnSrcFile->setIcon(QIcon(":/srcfile"));
    btnSrcFile->setToolTip(tr("Search File"));

    btnUpld = new QToolButton;
    btnUpld->setIcon(QIcon(":/ghost"));
    btnUpld->setToolTip(tr("Upload"));

    toolBar->addWidget(btnOpen);
    toolBar->addWidget(btnRemove);
    toolBar->addWidget(btnDel);
    toolBar->addWidget(btnReload);
    toolBar->addSeparator();
    toolBar->addWidget(btnSrcPkg);
    toolBar->addWidget(btnSrcFile);
    toolBar->addSeparator();
    toolBar->addWidget(btnUpld);


    connect(btnOpen, SIGNAL(clicked()), this, SLOT(installPkg()));
    connect(btnRemove, SIGNAL(clicked()), this, SLOT(removePkg()));
    connect(btnSrcPkg, SIGNAL(clicked()), this, SLOT(searchPkg()));
    connect(btnSrcFile, SIGNAL(clicked()), this, SLOT(searchFile()));
    connect(btnReload, SIGNAL(clicked()), this, SLOT(loadLocalPkgLists()));
    connect(btnDel, SIGNAL(clicked()), this, SLOT(deletePkg()));
    connect(btnUpld, SIGNAL(clicked()), this, SLOT(upld())); //NOTE Solo tasto TEST
}

void  PkMainWindow::tabLSelected(int selTab)
{
    if (selTab==0)                                  //is tab installed
        {
            btnOpen->setEnabled(true);
            btnOpen->setToolTip(tr("Packages Browser"));
            btnRemove->setEnabled(true);
            btnDel->setEnabled(false);
            btnSrcPkg->setEnabled(true);
            btnSrcFile->setEnabled(true);
            openAction->setEnabled(true);
            remPkgAction->setEnabled(true);
            delPkgAction->setEnabled(false);
            searchPkgAction->setEnabled(true);
            searchFileAction->setEnabled(true);

            pkSLine->setDisabled(false);

            setRTabs();
            instTreeView->clearSelection();
            pkSLine->setFocus();

        }
    else if(selTab==1)                              //is tab removed
        {
            btnOpen->setEnabled(false);
            btnRemove->setEnabled(false);
            btnDel->setEnabled(true);
            btnSrcPkg->setEnabled(false);
            btnSrcFile->setEnabled(false);
            openAction->setEnabled(false);
            remPkgAction->setEnabled(false);
            delPkgAction->setEnabled(true);
            searchPkgAction->setEnabled(false);
            searchFileAction->setEnabled(false);

            pkSLine->setDisabled(true);

            setRTabs();
            uninstTreeView->clearSelection();

        }
    else if(selTab==2)                            //is tab for repository
        {
            btnOpen->setEnabled(true);
            btnOpen->setToolTip(tr("Download Packages"));
            btnRemove->setEnabled(false);
            btnDel->setEnabled(false);
            btnSrcPkg->setEnabled(false);
            btnSrcFile->setEnabled(false);
            openAction->setEnabled(false);
            remPkgAction->setEnabled(false);
            delPkgAction->setEnabled(false);
            searchPkgAction->setEnabled(false);
            searchFileAction->setEnabled(false);

            setRTabs();
            upgTreeView->clearSelection();

            pkSLine->setDisabled(false);
        }
}

void PkMainWindow::setRTabs()
{
    pkSLine->clear();
    pkInfo->clear();
    pkFiles->clear();

    disconnect(rtabWidget,SIGNAL(currentChanged(int)), this, SLOT(tabRSelected(int)));

    rtabWidget->setTabVisible(0,true);
    rtabWidget->setTabVisible(1,false);
    rtabWidget->setTabVisible(2,false);
}
