/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKINFO_H
#define PKINFO_H

#include <QTextBrowser>
#include <QResizeEvent>

#include "pkinterface.h"
#include "pkstatusbar.h"

class PkInfo : public QTextBrowser
{
    Q_OBJECT
public:
    explicit PkInfo(QWidget *parent = 0);
    ~PkInfo();
    PkStatusBar *pkBar;

    void setSource(const QString &s);
    bool getPkgInfo(QString dataRow);

private:
    QString stmp, httpurl;
    PkInterface *pkInterface;
    void formatPkgInfo(QMap<QString, QString> infoPkg);
    qreal fontSize;

protected:
    void wheelEvent(QWheelEvent *event);

public slots:

private slots:
    void overUrl(QString strurl);
    void openUrl(QUrl url);

signals:
    void fontSizeChanged(int fontPSize);
};

#endif // PKINFO_H
