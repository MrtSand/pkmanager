/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QMessageBox>

#include "pkfindpkg.h"


PkFindPkg::PkFindPkg(QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle(tr("Find Package"));
    setWindowIcon(QIcon(":/srcpkg"));
    createForm();

    setModal(true);
}

PkFindPkg::~PkFindPkg()
{}

void PkFindPkg::createForm()
{
    QVBoxLayout *vtop = new QVBoxLayout(this);

    frame = new QGroupBox(tr("Find Package"), this);
    vtop->addWidget(frame, 1);
    QVBoxLayout *vf = new QVBoxLayout(frame);

    value = new QLineEdit(frame);
    vf->addWidget(value, 0);
    value->setFocus();
    value->setFixedHeight(value->sizeHint().height());
    connect(value, SIGNAL(returnPressed()), this, SLOT(ok_slot()));

    pkgCount = new QLabel(frame);
    pkgCount->setText(tr(" "));
    vf->addWidget(pkgCount, 0);

    hc = new QHBoxLayout();
    vf->addLayout(hc, 0);

    substr = new QCheckBox(tr("Sub string"), frame);
    substr->setChecked(true);
    hc->addWidget(substr, 1, Qt::AlignLeft);
    substr->setFixedSize(substr->sizeHint());
    hc->addStretch(1);

    showp = new QCheckBox(tr("Show Packages"));
    showp->setChecked(false);
    hc->addWidget(showp, 1, Qt::AlignRight);
    showp->setFixedSize(showp->sizeHint());

    hBox = new QHBoxLayout();
    vtop->addLayout(hBox, 0);

    ok = new QPushButton(tr("Find"), this);
    hBox->addWidget(ok, 1, Qt::AlignLeft);
    connect(ok, SIGNAL(clicked()), this, SLOT(ok_slot()));

    hBox->addStretch();

    cancel = new QPushButton(tr("Done"), this);
    hBox->addWidget(cancel, 1, Qt::AlignRight);
    connect(cancel, SIGNAL(clicked()), this, SLOT(done_slot()));

    setFixedSize(QSize(320, 160));
}

void PkFindPkg::ok_slot()
{

    QString pkgToSearch = value->text();
    if (pkgToSearch.length() >= 3)
        {
            doFind(pkgToSearch);
        }
    else
        {
            value->clear();
            QMessageBox::information(this, tr("Info "), tr("Enter more than three chars.."));
            value->setFocus();
        }
}

void PkFindPkg::doFind(QString &name)
{
    QStringList pkgFind;
    int nPkg;

    pkgCount->clear();

    pkgFind = pkTree->getItemSearch(name, substr->isChecked(), showp->isChecked());
    nPkg = pkgFind.count();
    if (nPkg)
        {
            pkgCount->setText(tr("Num: %1 Package Found").arg(QString::number(nPkg)));
        }
    else
        {
            QMessageBox::information(this, tr("Info "), tr("No Packages Found !"));
        }
}

void PkFindPkg::done_slot()
{
    close();
}

