/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKFINDPKG_H
#define PKFINDPKG_H

#include <QDialog>
#include <QPushButton>
#include <QButtonGroup>
#include <QLabel>
#include <QCheckBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "pktreeview.h"
#include "pkstatusbar.h"

class PkFindPkg : public QDialog
{
    Q_OBJECT
public:
    explicit PkFindPkg(QWidget *parent = 0);

    ~PkFindPkg();
    PkTreeView *pkTree;
    PkStatusBar *pkBar;

private:
    QPushButton *ok, *cancel;
    QCheckBox *substr;
    QCheckBox *showp;
    QGroupBox *frame;
    QLineEdit *value;
    QLabel *pkgCount;

    QVBoxLayout *vtop;
    QHBoxLayout *hc, *hBox;


private:
    void createForm();
    void doFind(QString &name);

signals:

public slots:

private slots:
    void done_slot();
    void ok_slot();


};

#endif // PKFINDPKG_H
