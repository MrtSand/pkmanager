/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QTimer>
#include <QMovie>
#include <QDebug>
#include "pkstatusbar.h"

PkStatusBar::PkStatusBar(QWidget *parent) :
    QStatusBar(parent)
{
    statusPBar = new QProgressBar();
    statusPBar->setRange(0, 100);
    statusPBar->setTextVisible(true);
    addPermanentWidget(statusPBar, 0);

    lbIcon = new QLabel();
    movie = new QMovie();
    addPermanentWidget(lbIcon,0);

    // hideWidgs();
}

PkStatusBar::~PkStatusBar()
{}
///
/// \brief PkStatusBar::stsConn
/// \param statusConn
///
void PkStatusBar::stsConn(bool statusConn)
{
    movie->stop();
    if(statusConn)
        {
            movie->setFileName(":/online");
        }
    else
        {
            movie->setFileName(":/offline");
        }
    lbIcon->setMovie(movie);
    movie->start();

}

void PkStatusBar::setPbText(QString datastr)
{
    if(!datastr.isNull())
        {
            queue.enqueue(datastr);

            while(!queue.isEmpty())
                {

                    this->showMessage(queue.dequeue(),3000);
                }
        }

}

void PkStatusBar::setProgValue(int value)
{
    statusPBar->show();
    statusPBar->setValue(value);
    if (value == statusPBar->maximum())
        {
            QTimer::singleShot(1000, this, SLOT(hideWidgs()));
        }
}

void PkStatusBar::hideWidgs()
{
    setPbText(QString());
    statusPBar->setValue(0);

    statusPBar->hide();
}

