﻿/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKSETUP_H
#define PKSETUP_H

#include <QDialog>
#include <QSettings>
#include <QString>
#include <QPalette>
#include <QStyle>
#include <QStyleFactory>

#include "ui_setup.h"
#include "pkstatusbar.h"

class PkSetup : public QDialog, public Ui::Dialog
{
    Q_OBJECT
public:
    explicit PkSetup(QWidget *parent = 0);
    ~PkSetup();

    void initSets();
    QVariant getSets(QString getGrp, QString getValue);
    void setSets(QString setGrp, QString setVar, QVariant setValue);

    QString getDefDir();
    QString getTmpDir();
    QString getFileListPkg();
    QString getRepo();
    QString getOsVer();
    QString getFullMirrorPath();
    QString getFullMirrorPatchPath();
    int getDays();
    int getStyle();

private:

    QSettings *pkSets;
    QStringList listTerm, listFMan;

    void initBtn();
    void initDialog();
    void loadSettings();
    void writeSettings();
    void loadMirrorList();
    void showMirrors(QStringList mirrors);

    enum Palette { light, dark };
    enum Theme {lightFusion, darkFusion };

    void changePalette(bool switchPal);

signals:
    void setupFinished();

public slots:

private slots:
    void tabSelected(int selTab);
    QString selDirTmp();
    void getItemSelected(QModelIndex item);
    void setStyle(int val);
    void mirrorFileDays(int days);
    void selVersion(int ver);
    void forceDnl();
    void ok_close();

protected:

};

#endif // PKSETUP_H
