/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKTREEVIEW_H
#define PKTREEVIEW_H

#include "pkstatusbar.h"
#include "pkinterface.h"

#include <QTreeView>
#include <QStandardItemModel>
#include <QString>
#include <QDateTime>
#include <QDir>
#include <QMenu>
#include <QAction>
#include <QActionGroup>
#include <QMessageBox>
#include <QXmlStreamWriter>
#include <QStringList>

#include <QDebug>

class SearchTreeProxyModel;

class PkTreeView : public QTreeView
{
    Q_OBJECT
public:
    explicit PkTreeView(QWidget *parent = 0);
    ~PkTreeView();
    PkInterface *pkInterface;
    PkStatusBar *pkBar;

    void filterData(const QString &filter);
    void loadFileList(const QString &parList);
    QStringList getItemsChecked();
    QStringList getItemSearch(QString &name, bool b_substr, bool b_showp);
    void setSelectItem(const QString &itemToSel);
    int pkgCount();
    void saveListPkg();

private:
    QStandardItemModel *model;
    SearchTreeProxyModel *proxyModel;
    QStandardItem *pkg_Item;
    QStandardItem *d_Item;
    QStandardItem *ck_Item;
    QStandardItem *st_Item;
    QStandardItem *dataInst_Hide;

    QString fData;
    int count;
    QString pkFileName, q;
    QDateTime pkFileDateTime;
    QDir dir;
    QPoint pos;
    QMenu menu0, menu1, menu2;

    QAction *resizeTree;
    QAction *sortingUp;
    QAction *sortingDw;
    QAction *uncheck;

    void createTree();
    void populateTree(QString repoPackages);
    void headerActions();

protected:
    virtual void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    virtual void keyPressEvent(QKeyEvent *event);

signals:
    void itemSel(QString retItem);
    void itemIsChanged(const QModelIndex &current);

public slots:
    void resizeCol();

private slots:
    void HeaderMenu(int section);
    void unCheckAll();
    void sortDataUp();
    void sortDataDw();

};

#include <QHeaderView>
#include <QMouseEvent>
class PkHeader :public QHeaderView
{
    Q_OBJECT
public:
    using QHeaderView::QHeaderView;
protected:
    void mousePressEvent(QMouseEvent *event)
    {
        QHeaderView::mousePressEvent(event);
        if(event->buttons() == Qt::RightButton)
            emit customSignal(logicalIndexAt(event->pos()));
    }
signals:
    void customSignal(int section);
};


#include <QSortFilterProxyModel>
class SearchTreeProxyModel : public QSortFilterProxyModel
{
public:
    SearchTreeProxyModel(QObject *parent = 0)
        : QSortFilterProxyModel(parent)
    {
        setFilterCaseSensitivity(Qt::CaseInsensitive);
    }

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
    {
        QModelIndex idx = sourceModel()->index(source_row, 0, source_parent);
        if (sourceModel()->hasChildren(idx))
            return (true);
        return (QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent));

    }
};

#endif // PKTREEVIEW_H
