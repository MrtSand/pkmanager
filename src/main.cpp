/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QtCore>
#include <QString>
#include <QStandardPaths>
#include <QTranslator>
#include <QSystemTrayIcon>

#include "pkconstants.h"
#include "pkmainwindow.h"

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);


#if defined(Q_OS_UNIX) && (QT_VERSION >= QT_VERSION_CHECK(5,1,0))
#include <QLockFile>
    QLockFile lockFile(QDir::temp().absoluteFilePath("<%1>.lock").arg(qAppName()));

    /* Trying to close the Lock File, if the attempt is unsuccessful for 100 milliseconds,
     * then there is a Lock File already created by another process.
     / Therefore, we throw a warning and close the program
     * */
    if(!lockFile.tryLock(10))
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText(PkConstants::msgLock());
            msgBox.exec();
            return 1;
        }
#endif

    app.setOrganizationName(PkConstants::getOrganizationName());
    app.setApplicationName(PkConstants::getApplicationName());

    if (!QSystemTrayIcon::isSystemTrayAvailable())
        {
            QMessageBox::critical(nullptr, QObject::tr("Systray"),
                                  QObject::tr("I couldn't detect any system tray "
                                              "on this system."));
            return 1;
        }

    QTranslator *pkTranslator = new QTranslator(&app);
    pkTranslator->load(":/translations/PkManager_" + QLocale::system().name()+".qm");
    if(!pkTranslator->isEmpty())
        {
            app.installTranslator(pkTranslator);
        }


    PkMainWindow mainWindow;
    mainWindow.show();
    return app.exec();
}
