/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#ifndef PKNETFUNC_H
#define PKNETFUNC_H

#include <QtCore>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTimer>

#include "pksetup.h"
#include "pkstatusbar.h"

class PkNetFunc : public QObject
{
    Q_OBJECT

public:
    explicit PkNetFunc(QWidget *parent = nullptr);
    ~PkNetFunc();

    PkSetup *pkSetup;

    bool manageUrlsMirror();
    void dnlHttpMirrors();

    bool manageMirrorPackageTxt();
    void doDownload(const QStringList &urls);
    void abort();

private:
    void parseHttpRemotePkgLists(QString packageList);
    void parseListPackage(QString fileParse);

    QNetworkAccessManager manager;

    void append(const QUrl &url);
    QString saveFileName(const QUrl &url);
    bool isHttpRedirect() const;
    void reportRedirect();

    QQueue<QUrl> downloadQueue;
    QNetworkReply *currentDownload = nullptr;
    QFile output;
    QElapsedTimer downloadTimer;

    QString timeHuman(const int time);
    QString sizeHuman(qint64 fileSize);

    int downloadedCount ;
    int totalCount ;
    QString size;

private slots:
    void startNextDownload();
    void downloadProgress(qint64 received, qint64 total);
    void downloadFinished();
    void downloadReadyRead();

signals:
    void dnwlFinished();

    void textToStBar(QString msg);
    void textToMngDnl(QString msg);
    void valueToMngDnl(int val);
    void valueSpeed(QString Ut);
};

#endif //PKNETFUNC_H
