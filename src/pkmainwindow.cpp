/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QShowEvent>
#include <QCloseEvent>
#include <QTimer>

#include <QDebug>

#include "pkmainwindow.h"
#include "pkconstants.h"


PkMainWindow::PkMainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    createForm();
    createActions();
    createMenu();
    createToolBar();
    initObj();
    variusConnect();

    setMouseTracking(true);
    resize(QSize(1024, 768));
    setWindowTitle(PkConstants::getApplicationName()+PkConstants::getVerApp());
    setWindowIcon(QIcon(":/pkmanager"));

    readSettings();

    setSysTrayIcon();

    connect(this, &PkMainWindow::window_loaded, [&]()
    {
        QTimer::singleShot(200, this, SLOT(initCoreApp()));
    });

    connect(pkSetup, SIGNAL(setupFinished()),this, SLOT(updateInfos()));
    connect(pkManage, SIGNAL(manageFinished()), this, SLOT(loadLocalPkgLists()));
    connect(pkManageDnl,SIGNAL(manageDnlFinished()),this,SLOT(loadRemTreePkgList()));

    pkSetup->initSets();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(callNetSts()));
    timer->start(3000);

    connect(pkUtility,SIGNAL(isOnLine(bool)),this,SLOT(stsInt(bool)));
}

PkMainWindow::~PkMainWindow() {}

///
/// \brief PkMainWindow::initCoreApp init load packages lists
///
void PkMainWindow::initCoreApp()
{
    loadReposPkgLists();

    loadLocalPkgLists();
}

///
/// \brief PkMainWindow::callNetSts check if internet is online
///
void PkMainWindow::callNetSts()
{
    pkUtility->netReplay();
}

///
/// \brief PkMainWindow::stsInt Icon statusbar is on/off-line
/// \param sts
///
void PkMainWindow::stsInt(bool sts)
{
    pkStatusBar->stsConn(sts);
}

void PkMainWindow::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);

    if (isOpen)
        return;
    isOpen = true;
    emit window_loaded();
}


