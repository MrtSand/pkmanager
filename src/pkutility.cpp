/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QFileDialog>
#include <QDialog>
#include <QProcess>
#include <QDebug>

#include "pkutility.h"
#include "pkconstants.h"

#include <stdio.h>
#include <stddef.h>

PkUtility::PkUtility(QObject *parent) : QObject(parent)
{
}

PkUtility::~PkUtility()
{}

QString PkUtility::osVersion()
{
    QFile OsVerFile("/etc/slackware-version");
    QString osVer;

    if(OsVerFile.exists())
        {
            OsVerFile.open(QIODevice::ReadOnly);
            osVer += QTextStream(&OsVerFile).readLine();
            OsVerFile.close();
        }
    else
        {
            osVer = tr("File %1 not found").arg(OsVerFile.fileName());
        }

    return osVer ;
}

QString PkUtility::sysArch()
{
    QProcess *sysarch = new QProcess();
    sysarch->setProcessChannelMode(QProcess::MergedChannels);
    sysarch->setProgram("arch");
    sysarch->start();

    sysarch->waitForFinished();

    QString ainfo = sysarch->readAllStandardOutput().simplified();
    sysarch->close();

    delete sysarch;

    QString osArchVer;

    if(ainfo == "x86")
        {
            osArchVer = osVersion().insert(9,"-").replace(" ","");
        }
    else if(ainfo == "x86_64")
        {
            osArchVer = osVersion().insert(9,"64-").replace(" ","");
        }

    return osArchVer;
}

QString PkUtility::sysArchCurrent()
{
    QString osArchVerR,sysA;
    sysA = sysArch();
    sysA.truncate(sysA.indexOf("-"));
    osArchVerR = sysA + "-current";

    return osArchVerR;
}

QString PkUtility::sysInfo()
{
    QProcess *sysInfo = new QProcess();
    sysInfo->setProcessChannelMode(QProcess::MergedChannels);
    sysInfo->setProgram("uname");
    QStringList sParam;
    sParam.append(QString("-a"));
    sysInfo->setArguments(sParam);
    sysInfo->start();

    sysInfo->waitForFinished();

    QString sinfo = sysInfo->readAllStandardOutput();
    sysInfo->close();

    delete sysInfo;

    return sinfo;
}

bool PkUtility::isExec(QString &isExeFile)
{
    QProcess proc;
    proc.setProcessChannelMode(QProcess::MergedChannels);
    QString sParam = QLatin1String("which ") + isExeFile;

    QStringList sl;
    sl.append(QLatin1String("-c"));
    sl.append(sParam);

    proc.setProgram("/bin/bash");
    proc .setArguments(sl);
    proc.start();
    proc.waitForFinished();

    QString out = QString::fromUtf8(proc.readAllStandardOutput());
    proc.close();

    if (out.isEmpty() || out.count(QStringLiteral("which")) > 0)
        {
            return false;
        }
    else
        {
            return true;
        }
}

void PkUtility::cmdTerm(QString dirofCmd, QString fileofCmd)
{
    QString term =  PkConstants::bTerm();
    QProcess *sh  = new QProcess(); //(qApp->activeWindow());
    sh->setProcessChannelMode(QProcess::MergedChannels);

    QString cArgs  = " -fn \"*-fixed-*-*-*-18-*\" -fg green -bg black -title xterm -e \"" ;

    if (!dirofCmd.isNull())
        {
            QString cmd = term + cArgs +
                          "cd " + dirofCmd + " && /bin/bash\"";
            sh->start(cmd);
        }
    if(!fileofCmd.isNull())
        {
            QString cmd = term + cArgs +
                          fileofCmd + " && /bin/bash\"";
            qInfo() << cmd;
            sh->start(cmd);

            /*  QString ag = cArgs + fileofCmd + " && /bin/bash\"";
              sh->setArguments(QStringList()<<ag);
              qInfo() << sh->readAllStandardOutput();
              sh->start();*/
        }

    sh->waitForFinished(-1);
}

#include <QtNetwork>
#include <QTimer>
#include <QNetworkConfigurationManager>

void PkUtility::netReplay()
{
    QUrl ulrToCeck(PkConstants::mirrorSite());

    QNetworkRequest request(ulrToCeck);

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    QNetworkReply *reply = manager.get(request);
    new Timeout(reply, 3000);

    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    if(reply->error() == QNetworkReply::NoError)
        {
            reply->bytesAvailable() ?  iSts = true : iSts = false;
        }
    else
        {
            iSts = false;
            qDebug() << reply->errorString();
        }

    reply->deleteLater();

    emit isOnLine(iSts);
}

