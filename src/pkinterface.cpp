/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QDir>
#include <QString>
#include <QTextStream>
#include <QMap>
#include <QRegExp>
#include <QDebug>

#include "pkinterface.h"
#include "pkconstants.h"

#define FILELIST "FILE LIST:\n"
#define HT "(http|https)://"
#define REG "(.*)[\\-](.*)[\\-](.*)[\\-](.*)"

PkInterface::PkInterface()
{
}

PkInterface::~PkInterface()
{}

///
/// \brief PkInterface::dirFileList Returns package list
/// \param repoPackages
/// \return
///
QFileInfoList PkInterface::dirFileList(QString repoPackages)
{
    QDir dir;
    QString fileInfo(repoPackages);
    dir.setPath(fileInfo);
    dir.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    // dir.setSorting(QDir::Name);

    QFileInfoList fileList = dir.entryInfoList();

    return fileList;
}

///
/// \brief PkInterface::numPkg Returns number of package installed
/// \param numPackages
/// \return
///
int PkInterface::numPkg(QString &numPackages)
{
    return dirFileList(numPackages).count();
}

///
/// \brief PkInterface::infoPkg Returns the info of package
/// \param info
/// \param dataRow
/// \return
///
bool PkInterface::infoPkg(QMap<QString, QString> &info, QString &dataRow)
{

    QString filePath, pkgName;
    QString infopkg;

    filePath = PkConstants::instListPKG();
    pkgName = dataRow;
    filePath += pkgName;

    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly))  //file opening and reading the content
        {
            return false ;
        }
    else
        {
            QString content = QTextStream(&file).readAll();
            //set the package description
            QStringList liststrings = content.split(FILELIST);

            infopkg =  QString();
            infopkg = (liststrings[0]);

            info =  getPkgInfo(infopkg);
        }
    file.close();
    return true ;
}
///
/// \brief PkInterface::getPkgInfo  pass package information to infoPkg
/// \param pkinp
/// \return
///
QMap<QString, QString> PkInterface::getPkgInfo(QString pkinp)
{
    QMap<QString, QString> a;
    QMap<QString, QString> group;

    group.insert("a", "A Base System");
    group.insert("ap", "AP Linux Applications");
    group.insert("d", "D Program Development");
    group.insert("e", "E GNU EMacs");
    group.insert("f", "F FAQs");
    group.insert("k", "K Kernel Source");
    group.insert("kde", "KDE System");
    group.insert("kdei", "KDE Lang.");
    group.insert("l", "L Libraries");
    group.insert("n", "N Networking");
    group.insert("t", "TeX Distribution");
    group.insert("tcl", "TCL Script Language");
    group.insert("x", "X Window System");
    group.insert("xap", "X Applications");
    group.insert("y", "Y Games");

    char *str, *xstr;

    char *inp = qstrdup(pkinp.toLatin1());      //qstrdup dupl char
    str = strtok(inp, "\n");                    //strtok  divide  \n a-capo

    if (str)
        {
            do
                {
                    if (str[0] == 0)
                        break;
                    xstr = strchr(str, ':');          //locates the first occurrence of a specified character
                    if (xstr)
                        {
                            *xstr++ = 0;
                            xstr++;
                            while (*xstr == ' ')
                                {
                                    xstr++;
                                }
                            for (int i = 0; str[ i ] != '\0'; ++i)
                                str[i] = tolower(str[i]);;
                            if (*str == ' ')
                                str++;

                            // name + version + arch
                            if (!strcmp("package name", str))
                                {
                                    QString st = xstr;
                                    a.insert("name",st);

                                    QRegExp exp(REG);
                                    int pos = 0 ;

                                    pos = exp.indexIn(st);

                                    while((pos=exp.indexIn(st,pos)) != -1)
                                        {
                                            a.insert("version",exp.cap(2));
                                            a.insert("arch",exp.cap(3));

                                            pos +=exp.matchedLength();
                                        }
                                }

                            // size compressed
                            else if (!strcmp("compressed package size", str) ||
                                     !strcmp("package size (compressed)", str))
                                {
                                    QString stmp = xstr;
                                    a.insert("comp-size", stmp);
                                }

                            // size uncompressed
                            else if (!strcmp("uncompressed package size", str) ||
                                     !strcmp("package size (uncompressed)", str))
                                {
                                    QString stmp = xstr;
                                    a.insert("uncomp-size", stmp);
                                }

                            // group
                            else if (!strcmp("package location", str))
                                {
                                    QString sl = xstr;
                                    int inc;
                                    int sls = sl.lastIndexOf("/");
                                    int dirLL = PkConstants::instListPKG().length();
                                    if (sls >= dirLL)
                                        {
                                            for (inc = 1; sl[ sls - inc ] != '/'; ++inc)
                                                {}
                                            inc--;
                                            QString gt = sl.mid(sls - inc, inc);
                                            a.insert("group", group[gt]);
                                        }
                                    else
                                        {
                                            a.insert("group", "No Group");
                                        }
                                }

                            // summary + description + url
                            else if (!strcmp("package description", str))
                                {
                                    int i = 0;
                                    QString qstr = "";

                                    while ((str = strtok(NULL, "\n")))
                                        {
                                            xstr = strchr(str, ':');
                                            if (xstr)
                                                {
                                                    *xstr++ = 0;
                                                    if (*(xstr) != 0)
                                                        xstr++;
                                                    while (*xstr == ' ')
                                                        {
                                                            xstr++;
                                                        }
                                                    if (i == 0)
                                                        {
                                                            a.insert("summary", xstr);
                                                        }
                                                    else
                                                        {
                                                            if (!strcmp(xstr, "") && (i != 1))
                                                                {
                                                                    qstr += "\n";
                                                                }
                                                            else
                                                                {
                                                                    qstr += xstr;
                                                                    qstr += " ";
                                                                }
                                                        }
                                                }
                                            QString tmpstr = QString::fromLatin1(xstr);
                                            QRegExp ht(HT);
                                            if(tmpstr.contains(ht))
                                                {
                                                    int intUrl = tmpstr.lastIndexOf(ht);
                                                    QString url = tmpstr.mid(intUrl);
                                                    if (url.endsWith("/"))
                                                        {
                                                            int leng = url.length() - 1;
                                                            url.remove(leng, 1);
                                                            a.insert("http", url);
                                                        }
                                                    else
                                                        {
                                                            a.insert("http", url);
                                                        }
                                                }
                                            i++;
                                        }
                                    a.insert("description", qstr.simplified());
                                }

                            else
                                {
                                    a.insert(str, xstr);
                                }
                        }
                }
            while ((str = strtok(NULL, "\n")));
        }
    delete [] inp;
    inp = NULL;

    return a;
}
///
/// \brief PkInterface::infoFileList Return the files iside package
/// \param fileList
/// \param dataRow
/// \return
///
bool PkInterface::infoFileList(QStringList &fileList, QString dataRow)
{
    QString filePath, pkgName;
    QString infofilepkg;

    filePath = PkConstants::instListPKG();
    pkgName = dataRow;
    filePath += pkgName;

    //file opening and reading the content
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly))
        {
            return false ;
        }
    else
        {
            QString content = QTextStream(&file).readAll();
            //set the package description
            QStringList liststrings = content.split(FILELIST);

            infofilepkg = QString();
            infofilepkg = (liststrings[1]);

            fileList =  formatFileInfo(infofilepkg);
        }
    file.close();
    return true ;
}
///
/// \brief PkInterface::formatFileInfo pass package files to infoFileList
/// \param _inp
/// \return
///
QStringList PkInterface::formatFileInfo(QString _inp)
{
    QStringList inp, out;

    inp = _inp.split('\n', Qt::SkipEmptyParts);

    for (QStringList::Iterator it =  inp.begin(); it !=  inp.end(); ++it)
        {
            QString current = "/" + *it;

            if (!current.contains("/./"))
                {
                    if (current.left(8) != "/install")
                        {
                            out.append(current);
                        }
                }
        }

    return out;
}
