/************************************************************************
*   PkManager Package Manager for Slackware                             *
*   Copyright (C)   MrtSand                                             *
*   https://gitlab.com/MrtSand/pkmanager.git                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or       *
*   modify it under the terms of the GNU General Public License         *
*   as published by the Free Software Foundation; either version 2      *
*   of the License, or (at your option) any later version.              *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the Free Software         *
*   Foundation, Inc., 51 Franklin Street, Fifth Floor,                  *
*   Boston, MA  02110-1301, USA.                                        *
************************************************************************/

#include <QFileInfoList>
#include <QMap>

#ifndef PKINTERFACE_H
#define PKINTERFACE_H

class PkInterface
{
public:
    PkInterface();
    ~PkInterface();

    QFileInfoList dirFileList(QString repoPackages);
    int numPkg(QString &numPackages);
    bool infoPkg(QMap<QString, QString> &info, QString &dataRow);
    bool infoFileList(QStringList &fileList, QString dataRow);

private:

    QMap<QString, QString> getPkgInfo(QString pkinp);
    QStringList formatFileInfo(QString _inp);

};

#endif // PKINTERFACE_H
